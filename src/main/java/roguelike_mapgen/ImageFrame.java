package roguelike_mapgen;

import javax.swing.*;

public class ImageFrame extends JFrame {
    static final Integer DEFAULT_WIDTH = 640;
    static final Integer DEFAULT_HEIGHT = 480;

    public ImageFrame() {
        setTitle("Map Generator");
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);

        ImageComponent imageComponent = new ImageComponent();
        add(imageComponent);
        getContentPane().validate();
        getContentPane().repaint();
    }
}
