package roguelike_mapgen;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class ImageComponent extends JComponent {
    private Image image;

    public ImageComponent() {
        try {
            File imageFile = new File("resources/img/test.png");
            image = ImageIO.read(imageFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void paintComponent(Graphics g) {
        if (image == null) {
            return;
        }

        g.drawImage(image, 0, 0, this);
    }
}
