package roguelike_mapgen;

import javax.swing.*;
import java.awt.*;
//import java.awt.*;

public class Main {
    public static void main(String... args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setTitle("Map Generator");
        frame.setSize(800, 640);
        frame.setLayout(new GridLayout(1,2));

        ImageComponent imageComponent = new ImageComponent();
        frame.add(imageComponent);

        JPanel controls = new JPanel();
        controls.setLayout(new GridLayout(0, 2));
        frame.add(controls);

        controls.add(new JButton("Button 1"));
        controls.add(new JButton("Button 2"));
        controls.add(new JButton("Button 3"));
        controls.add(new JButton("Button 4"));
        controls.add(new JComboBox<Integer>(new Integer[]{3, 2, 4}));
        controls.add(new JButton("Button 5"));


        frame.getContentPane().validate();
        frame.getContentPane().repaint();

        frame.setVisible(true);
    }
}
