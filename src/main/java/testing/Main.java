package testing;

import com.googlecode.lanterna.Symbols;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextCharacter;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;
import com.googlecode.lanterna.screen.TerminalScreen;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.googlecode.lanterna.terminal.Terminal;
import roguelike.core.*;
import roguelike.graphics.game.Camera;
import roguelike.text.Text;

import java.io.IOException;
import java.security.Key;
import java.util.ArrayList;
import java.util.List;

public class Main {

    Terminal terminal;
    TerminalScreen screen;
    TextGraphics textGraphics;

    double[][] values;
    double[][] mask;

    List<Pair<Double, TextColor>> heightColors;

    final int WIDTH = 32;
    final int HEIGHT = 32;

    public static void main(String... args) {
        try {
            new Main().run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void run() throws IOException {
        DefaultTerminalFactory defaultTerminalFactory = new DefaultTerminalFactory();
        defaultTerminalFactory.setInitialTerminalSize(
                new TerminalSize(WIDTH * 2, HEIGHT));

        terminal = defaultTerminalFactory.createTerminal();
        screen = new TerminalScreen(terminal);
        textGraphics = screen.newTextGraphics();

        values = new double[WIDTH][HEIGHT];
        mask = new double[WIDTH][HEIGHT];
        double maxLength = (WIDTH / 2.0) * (WIDTH / 2.0) + (HEIGHT / 2.0) * (HEIGHT / 2.0);
        maxLength = Math.sqrt(maxLength);
        for (int i = 0; i < WIDTH; ++i) {
            for (int j = 0; j < HEIGHT; ++j) {

                double dx = WIDTH / 2.0 - i;
                double dy = HEIGHT / 2.0 - j;

                double value = 1.0 - Math.sqrt(dx * dx + dy * dy) / maxLength;
                mask[i][j] = Math.sin(value * CommonMath.HALF_PI);

            }
        }

        Noise.addNoise("base", 4, 0.5, 2.5);

        heightColors = new ArrayList<Pair<Double, TextColor>>();
        heightColors.add(new Pair(0.0, new TextColor.RGB(0, 0, 80)));
        heightColors.add(new Pair(0.3, new TextColor.RGB(0, 0, 195)));
        heightColors.add(new Pair(0.4, new TextColor.RGB(180, 180, 0)));
        heightColors.add(new Pair(0.5, new TextColor.RGB(80, 180, 0)));
        heightColors.add(new Pair(0.7, new TextColor.RGB(0, 100, 0)));

        double scale = 0.1;
        double startX = 0.0;
        double startY = 0.0;
        boolean useMask = true;

        screen.startScreen();

        boolean needsUpdate = true;

        while(true) {

            KeyStroke keyStroke;
            while ((keyStroke = terminal.pollInput()) != null) {
                if (keyStroke.getKeyType() == KeyType.Character) {
                    switch (keyStroke.getCharacter()) {
                        case 'w':
                            startY -= scale;
                            break;
                        case 'a':
                            startX -= scale;
                            break;
                        case 's':
                            startY += scale;
                            break;
                        case 'd':
                            startX += scale;
                            break;
                        case '+':
                            scale /= 0.5;
                            break;
                        case '-':
                            scale *= 0.5;
                            break;
                        case 'm':
                            useMask = !useMask;
                            break;
                    }

                    needsUpdate = true;
                }
            }

            if (needsUpdate) {
                for (int i = 0; i < WIDTH; ++i) {
                    for (int j = 0; j < HEIGHT; ++j) {

                        double x = startX + i * scale;
                        double y = startY + j * scale;

                        double value;
                        if (useMask) {
                            value = Noise.get2D("base", x, y) * mask[i][j];
                        } else {
                            value = Noise.get2D("base", x, y);
                        }

                        values[i][j] = value;

                    }
                }

                for (int i = 0; i < 5; ++i) {
                    int x = Random.getInteger(WIDTH);
                    int y = Random.getInteger(HEIGHT);
                    if (values[x][y] > 0.6) {

                        for (int r = 0; r < 100; ++r) {

                            if (x < 0 || x >= WIDTH || y < 0 || y >= HEIGHT) {
                                break;
                            }

                            if (values[x][y] < 0.4) {
                                break;
                            }



                            values[x][y] = 0.2;
                            double up = (y > 0) ? values[x][y - 1] : 100.0;
                            up = (up > 0.2) ? up : 100.0;
                            double left = (x > 0) ? values[x - 1][y] : 100.0;
                            left = (left > 0.2) ? left : 100.0;
                            double down = (y < HEIGHT - 1) ? values[x][y + 1] : 100.0;
                            down = (down > 0.2) ? down : 100.0;
                            double right = (x < WIDTH - 1) ? values[x + 1][y] : 100.0;
                            right = (right > 0.2) ? right : 100.0;

                            double min = CommonMath.min(up, left, down, right);

                            if (up == min) {
                                y -= 1;
                            } else if (left == min) {
                                x -= 1;
                            } else if (down == min) {
                                y += 1;
                            } else {
                                x += 1;
                            }
                        }

                    }
                }

                for (int i = 0; i < WIDTH; ++i) {
                    for (int j = 0; j < HEIGHT; ++j) {

                        double value = values[i][j];

                        TextColor.RGB color = (TextColor.RGB) heightColors.get(0).getSecond();
                        int h = 1;
                        while (h < heightColors.size() && heightColors.get(h).getFirst() < value) {
                            color = (TextColor.RGB) heightColors.get(h).getSecond();
                            ++h;
                        }

                        if (h > 0 && h != 2 && h < heightColors.size()) {
                            TextColor.RGB nextColor = (TextColor.RGB) heightColors.get(h).getSecond();
                            double baseHeight = heightColors.get(h - 1).getFirst();
                            double nextHeight = heightColors.get(h).getFirst();
                            double t = (value - baseHeight) / (nextHeight - baseHeight);

                            int r = (int) CommonMath.lerp(t, color.getRed(), nextColor.getRed());
                            int g = (int) CommonMath.lerp(t, color.getGreen(), nextColor.getGreen());
                            int b = (int) CommonMath.lerp(t, color.getBlue(), nextColor.getBlue());

                            color = new TextColor.RGB(r, g, b);
                        }

                        textGraphics.setCharacter(i * 2, j, new TextCharacter(
                                Symbols.BLOCK_SOLID, color, new TextColor.RGB(0, 0, 0)));
                        textGraphics.setCharacter(i * 2 + 1, j, new TextCharacter(
                                Symbols.BLOCK_SOLID, color, new TextColor.RGB(0, 0, 0)));

                    }
                }

                screen.refresh();

                needsUpdate = false;

            }

        }

    }
}
