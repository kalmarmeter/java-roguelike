package roguelike.player;

import roguelike.core.Vector2i;
import roguelike.entity.Attributes;
import roguelike.entity.Entity;
import roguelike.weapon.weapons.AssaultRifle;
import roguelike.weapon.weapons.BoltActionRifle;

public class Player extends Entity {

    public Player(Vector2i position) {
        super(position);
        attributes = new Attributes(6, 10);
        currentWeapon = new AssaultRifle();
    }

    @Override
    public String toString() {
        return "Player";
    }
}
