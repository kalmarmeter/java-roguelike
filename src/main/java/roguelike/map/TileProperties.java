package roguelike.map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import roguelike.core.Debug;

import java.io.File;

public class TileProperties {
    Boolean solid = false;
    TileHeight height = TileHeight.FULL;
    Integer maxHP = 999;
    Integer baseMoveCost = 999;
    Boolean flammable = false;
    TileType destroyedTile = TileType.DIRT;

    public static final TileProperties DEFAULT_PROPERTIES = new TileProperties();

    static {
        loadTileProperties();
    }

    static java.util.Map<TileType, TileProperties> tilePropertiesMap;

    public TileProperties() {

    }

    public TileProperties(Boolean solid, TileHeight height, Integer maxHP, Integer baseMoveCost, Boolean flammable, TileType destroyedTile) {
        this.solid = solid;
        this.height = height;
        this.maxHP = maxHP;
        this.baseMoveCost = baseMoveCost;
        this.flammable = flammable;
        this.destroyedTile = destroyedTile;
    }

    public static void loadTileProperties() {
        tilePropertiesMap = new java.util.HashMap<TileType, TileProperties>();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(new File("resources/json/tile_properties.json"));
            for (TileType tileType : TileType.values()) {
                TileProperties tileProperties = new TileProperties();
                JsonNode tile = root.get(tileType.toString());
                if (tile != null) {
                    tileProperties.solid = tile.get("solid").asBoolean(DEFAULT_PROPERTIES.solid);
                    tileProperties.height = TileHeight.valueOf(tile.get("height").asText(DEFAULT_PROPERTIES.height.toString()));
                    tileProperties.maxHP = tile.get("maxHP").asInt(DEFAULT_PROPERTIES.maxHP);
                    tileProperties.baseMoveCost = tile.get("baseMoveCost").asInt(DEFAULT_PROPERTIES.baseMoveCost);
                    tileProperties.flammable = tile.get("flammable").asBoolean(DEFAULT_PROPERTIES.flammable);
                    tileProperties.destroyedTile = TileType.valueOf(tile.get("destroyedTile").asText(DEFAULT_PROPERTIES.destroyedTile.toString()));
                }
                tilePropertiesMap.put(tileType, tileProperties);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static TileProperties getTileProperty(TileType type) {
        return tilePropertiesMap.get(type);
    }

    public static TileProperties getTileProperty(Tile tile) {
        return getTileProperty(tile.getType());
    }

    public boolean isSolid() {
        return solid;
    }

    public TileHeight getHeight() {
        return height;
    }

    public Integer getMaxHP() {
        return maxHP;
    }

    public Integer getBaseMoveCost() {
        return baseMoveCost;
    }

    public Boolean isFlammable() {
        return flammable;
    }

    public TileType getDestroyedTile() {
        return destroyedTile;
    }
}
