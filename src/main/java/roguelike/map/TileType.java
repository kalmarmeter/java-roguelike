package roguelike.map;

public enum TileType {
    EMPTY,
    DIRT,
    GRASS,
    SNOW,
    MUD,
    WATER,
    WALL,
    OAK_TREE,
    PINE_TREE,
    BRANCHES,
    RUBBLE
}
