package roguelike.map;

import roguelike.core.Pair;
import roguelike.core.Random;
import roguelike.core.Vector2i;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Building {
    static List<Building> buildings = new ArrayList<Building>();

    static public void loadBuildings() {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("resources/mapgen/buildings.txt"));
            Building building = new Building();
            String line = reader.readLine();
            while (line != null) {
                int width = 0;
                int height = 0;
                if (!line.equals("-")) {
                    width = line.length();
                }
                while (line != null && !line.equals("-")) {
                    for (int i = 0; i < width; ++i) {
                        TileType type = TileType.EMPTY;
                        switch (line.charAt(i)) {
                            case 'w':
                                type = TileType.WALL;
                                break;
                            case 't':
                                type = TileType.OAK_TREE;
                                break;
                            case '.':
                                type = TileType.GRASS;
                                break;
                        }

                        building.tiles.add(new Pair<Vector2i, TileType>(new Vector2i(i, height), type));
                    }

                    height++;

                    line = reader.readLine();
                }

                if (width != 0 && height != 0) {
                    building.width = width;
                    building.height = height;
                    buildings.add(building);
                    building = new Building();
                }

                line = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    public static Building randomBuilding() {
        return buildings.get(Random.getInteger(buildings.size()));
    }

    Integer width;
    Integer height;
    Set<Pair<Vector2i, TileType>> tiles = new HashSet<Pair<Vector2i, TileType>>();

    public Building() {

    }

    public Integer getWidth() {
        return width;
    }

    public Integer getHeight() {
        return height;
    }

    public Set<Pair<Vector2i, TileType>> getTiles() {
        return tiles;
    }
}
