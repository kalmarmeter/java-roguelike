package roguelike.map;

import roguelike.main.Game;
import roguelike.core.Random;
import roguelike.core.Rectanglei;
import roguelike.core.Vector2i;
import roguelike.entity.Entity;
import roguelike.graphics.game.AnimationAttributes;
import roguelike.graphics.game.animations.SmokeAnimation;
import roguelike.ray.LightTracer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Map {
    Game game;
    Chunk[][] chunks;
    Rectanglei mapArea;
    List<Vector2i> lights;
    LightTracer lightTracer;
    public final Integer WIDTH_CHUNKS = 4;
    public final Integer HEIGHT_CHUNKS = 4;
    public final Integer WIDTH = WIDTH_CHUNKS * Chunk.WIDTH;
    public final Integer HEIGHT = HEIGHT_CHUNKS * Chunk.HEIGHT;

    public Map(Game game) {
        this.game = game;
        mapArea = new Rectanglei(Vector2i.ZERO, new Vector2i(WIDTH, HEIGHT));
        chunks = new Chunk[WIDTH_CHUNKS][HEIGHT_CHUNKS];
        Building.loadBuildings();
        Rectanglei chunksRect = new Rectanglei(Vector2i.ZERO, new Vector2i(WIDTH_CHUNKS, HEIGHT_CHUNKS));
        for (Vector2i chunkIndexPosition : chunksRect) {
            Vector2i chunkPosition = new Vector2i(chunkIndexPosition.getX() * Chunk.WIDTH,
                                                    chunkIndexPosition.getY() * Chunk.HEIGHT);
            chunks[chunkIndexPosition.getX()][chunkIndexPosition.getY()] = new Chunk(chunkPosition);
        }
        this.lights = new ArrayList<Vector2i>();
        lightTracer = new LightTracer(this);
    }

    public void generate() {
        for (Chunk[] chunkColumn : chunks) {
            for (Chunk chunk : chunkColumn) {
                chunk.generate();
            }
        }
    }

    public Chunk getChunkAt(Vector2i position) {
        return chunks[position.getX() / Chunk.WIDTH][position.getY() / Chunk.HEIGHT];
    }

    public Vector2i getChunkIndexAt(Vector2i position) {
        return new Vector2i(position.getX() / Chunk.WIDTH, position.getY() / Chunk.HEIGHT);
    }

    public Tile getTile(Vector2i position) {
        if (mapArea.contains(position)) {
            Chunk chunk = getChunkAt(position);
            return chunk.getTile(position);
        } else {
            return null;
        }
    }

    public boolean isSolidTile(Vector2i position) {
        return TileProperties.getTileProperty(getTile(position)).isSolid();
    }

    public boolean isFlammableTile(Vector2i position) {
        return TileProperties.getTileProperty(getTile(position)).isSolid();
    }

    public void setTile(Vector2i position, Tile newTile) {
        if (mapArea.contains(position)) {
            Chunk chunk = getChunkAt(position);
            chunk.setTile(position, newTile);
        }
    }

    public Rectanglei getMapArea() {
        return mapArea;
    }

    public Entity entityAt(Vector2i position) {
        if (!mapArea.contains(position)) {
            return null;
        }

        return getChunkAt(position).getEntityAt(position);
    }

    public void explode(Vector2i center, Integer radius) {
        Rectanglei area = new Rectanglei(center.subtract(new Vector2i(radius, radius)), new Vector2i(radius * 2, radius * 2));
        for (Vector2i pos : area) {
            Tile tile = getTile(pos);
            if (tile != null) {
                if (pos.distanceTo(center) <= radius) {
                    Integer damage = Random.getInteger(40, 50);
                    tile.damage(damage);
                    if (tile.isDestroyed()) {
                        setTile(pos, new Tile(TileProperties.getTileProperty(tile).getDestroyedTile()));
                    } else {
                        if (TileProperties.getTileProperty(tile).isFlammable() && Random.oneIn(2)) {
                            tile.setOnFire(true);
                        }
                    }
                }
            }
        }
    }

    public void updateLighting() {
        lights.clear();
        for (Entity entity : game.getEntities()) {
            lights.add(entity.getPosition());
        }
        for (Vector2i pos : mapArea) {
            getTile(pos).setLightLevel(0);
        }
        lightTracer.traceLights(lights);
    }

    public void updateTiles() {
        updateLighting();
        for (Vector2i position : mapArea) {
            Tile tile = getTile(position);
            if (tile.isOnFire()) {

                if (Random.oneIn(2)) {
                    game.getWindow().getGameRenderer().addAnimation(new SmokeAnimation(), new AnimationAttributes(position, 1));
                }

                tile.damage(3);
                if (tile.isDestroyed()) {
                    setTile(position, new Tile(TileProperties.getTileProperty(tile).getDestroyedTile()));
                    continue;
                }

                if (Random.oneIn(10)) {
                    tile.setOnFire(false);
                } else {
                    List<Vector2i> neighbors = position.getNeighbours();
                    for (Vector2i neighborPos : neighbors) {
                        Tile neighborTile = getTile(neighborPos);
                        if (neighborTile != null) {
                            if (!neighborTile.isOnFire() && TileProperties.getTileProperty(neighborTile).isFlammable()) {
                                if (Random.oneIn(2)) {
                                    neighborTile.setOnFire(true);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
