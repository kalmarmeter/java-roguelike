package roguelike.map;

import roguelike.core.Random;
import roguelike.entity.Entity;

import java.util.Set;

public class Tile {

    public static final Tile EMPTY = new Tile(TileType.EMPTY);

    TileType type;
    Integer hp;
    Boolean onFire;
    Boolean bloody;
    Boolean flammable;
    Boolean destroyed;
    Integer lightLevel;
    Integer randomID;

    public Tile(TileType type) {
        this.type = type;
        TileProperties properties = TileProperties.getTileProperty(type);
        this.hp = properties.getMaxHP();
        this.flammable = properties.isFlammable();
        this.onFire = false;
        this.destroyed = false;
        this.randomID = Random.getInteger();
        this.bloody = false;
        this.lightLevel = 0;
    }

    public TileType getType() {
        return type;
    }

    public Boolean isOnFire() {
        return onFire;
    }

    public void setOnFire(boolean onFire) {
        this.onFire = onFire;
    }

    public Boolean isDestroyed() {
        return destroyed;
    }

    public Boolean isBloody() {
        return bloody;
    }

    public void setBloody(Boolean bloody) {
        this.bloody = bloody;
    }

    public Integer getLightLevel() {
        return lightLevel;
    }

    public void setLightLevel(Integer lightLevel) {
        this.lightLevel = lightLevel;
    }

    public Boolean damage(Integer damage) {
        this.hp -= damage;
        if (hp <= 0) {
            this.destroyed = true;
        }

        return destroyed;
    }

    public Integer getRandomID() {
        return randomID;
    }
}
