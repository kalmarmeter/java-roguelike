package roguelike.map;

public enum TileHeight {
    GROUND,
    HALF,
    FULL
}
