package roguelike.map;

import roguelike.core.*;
import roguelike.entity.Entity;

import java.util.HashSet;
import java.util.Set;

public class Chunk {
    public static final Integer WIDTH = 16;
    public static final Integer HEIGHT = 16;

    static LayeredNoise vegetationNoise = Noise.addNoise("Vegetation");
    static LayeredNoise temperatureNoise = Noise.addNoise("Temperature");

    Tile[][] tiles;
    Vector2i position;
    Rectanglei chunkArea;
    Rectanglei chunkRelativeArea;
    Set<Entity> entities;

    Chunk(Vector2i position) {
        this.position = position;
        entities = new HashSet<Entity>();
        tiles = new Tile[WIDTH][HEIGHT];
        chunkArea = new Rectanglei(position, new Vector2i(WIDTH, HEIGHT));
        chunkRelativeArea = new Rectanglei(Vector2i.ZERO, new Vector2i(WIDTH, HEIGHT));
    }

    public Vector2i getPosition() {
        return position;
    }

    public Rectanglei getChunkArea() {
        return chunkArea;
    }

    public void setTile(Vector2i point, Tile newTile) {
        setRelativeTile(chunkArea.getRelativePosition(point), newTile);
    }

    public void setRelativeTile(Vector2i relativePoint, Tile newTile) {
        tiles[relativePoint.getX()][relativePoint.getY()] = newTile;
    }

    public void generate() {
        for (Vector2i relativePosition : chunkRelativeArea) {
            /*
            Integer r = Random.getInteger(100);
            if (r == 0) {
                setRelativeTile(position, new Tile(TileType.WALL));
            } else if (r < 5) {
                setRelativeTile(position, new Tile(TileType.TREE));
            } else {
                setRelativeTile(position, new Tile(TileType.GRASS));
            }
            */
            Vector2i position = chunkArea.getGlobalPosition(relativePosition);
            double value = vegetationNoise.get2D(position.getX() / 10.0, position.getY() / 10.0);
            double temperature = temperatureNoise.get2D(position.getX() / 20.0, position.getY() / 20.0);
            if ((value > 0.5 && Random.oneIn(5)) || (value > 0.3  && Random.oneIn(10))) {
                if (temperature < -1.0) {
                    setRelativeTile(relativePosition, new Tile(TileType.PINE_TREE));
                } else {
                    setRelativeTile(relativePosition, new Tile(TileType.OAK_TREE));
                }
            } else {
                if (temperature < -1.0) {
                    setRelativeTile(relativePosition, new Tile(TileType.SNOW));
                } else {
                    setRelativeTile(relativePosition, new Tile(TileType.GRASS));
                }
            }
        }

        generateBuildings();
    }

    private void generateBuildings() {
        if (Random.oneIn(2)) {
            Building building = Building.randomBuilding();
            Vector2i position = new Vector2i(Random.getInteger(chunkArea.left(), chunkArea.right() - building.getWidth()),
                    Random.getInteger(chunkArea.top(), chunkArea.bottom() - building.getHeight()));
            for (Pair<Vector2i, TileType> pair : building.getTiles()) {
                setTile(position.add(pair.getFirst()), new Tile(pair.getSecond()));
            }
        }
    }

    public Tile getTile(Vector2i point) {
        if (chunkArea.contains(point)) {
            return getTileRelative(chunkArea.getRelativePosition(point));
        } else {
            return null;
        }
    }

    public Tile getTileRelative(Vector2i relativePoint) {
        return tiles[relativePoint.getX()][relativePoint.getY()];
    }

    public void addEntity(Entity entity) {
        entities.add(entity);
    }

    public void removeEntity(Entity entity) {
        entities.remove(entity);
    }

    public Entity getEntityAt(Vector2i position) {
        for (Entity entity : entities) {
            if (entity.getPosition().equals(position)) {
                return entity;
            }
        }
        return null;
    }
}
