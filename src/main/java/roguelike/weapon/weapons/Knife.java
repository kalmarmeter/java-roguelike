package roguelike.weapon.weapons;

import roguelike.core.time.TimeDuration;
import roguelike.weapon.MeleeWeapon;

public class Knife extends MeleeWeapon {
    @Override
    public String toString() {
        return "Knife";
    }

    public Integer getBaseDamage() {
        return 40;
    }

    public Integer getDamageVariance() {
        return 5;
    }

    public TimeDuration getBaseCooldownTime() {
        return TimeDuration.seconds(3);
    }

    public Integer getMaxRange() {
        return 1;
    }

    public String useSound() {
        return "knife_slash";
    }
}
