package roguelike.weapon.weapons;

import roguelike.core.time.TimeDuration;
import roguelike.weapon.Gun;
import roguelike.weapon.Weapon;

public class BoltActionRifle extends Gun {
    @Override
    public String toString() {
        return "Bolt Action Rifle";
    }

    public Double getInaccuracy() {
        return 7.0;
    }

    public Integer getMaxAmmo() {
        return 5;
    }

    public TimeDuration getBaseReloadTime() {
        return TimeDuration.seconds(5);
    }

    public Integer getBaseDamage() {
        return 40;
    }

    public Integer getDamageVariance() {
        return 8;
    }

    public TimeDuration getBaseCooldownTime() {
        return TimeDuration.seconds(2);
    }

    public Integer getMaxRange() {
        return 12;
    }

    public String useSound() {
        return "bolt_shoot";
    }
}
