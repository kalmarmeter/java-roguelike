package roguelike.weapon.weapons;

import roguelike.core.time.TimeDuration;
import roguelike.weapon.Gun;
import roguelike.weapon.Weapon;

public class AssaultRifle extends Gun {

    @Override
    public String toString() {
        return "Assault Rifle";
    }

    public Double getInaccuracy() {
        return 15.0;
    }

    public Integer getMaxAmmo() {
        return 30;
    }

    public TimeDuration getBaseReloadTime() {
        return TimeDuration.seconds(4);
    }

    public Integer getBaseDamage() {
        return 20;
    }

    public Integer getDamageVariance() {
        return 5;
    }

    public TimeDuration getBaseCooldownTime() {
        return TimeDuration.seconds(0);
    }

    public Integer getMaxRange() {
        return 10;
    }

    public String useSound() {
        return "ar_shoot";
    }
}
