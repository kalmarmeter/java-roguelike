package roguelike.weapon;

import roguelike.core.CommonMath;
import roguelike.core.Pair;
import roguelike.core.Random;
import roguelike.core.time.TimeDuration;
import roguelike.core.time.TimePoint;
import roguelike.main.Action;

public abstract class Weapon {
    protected TimePoint lastUsed = TimePoint.ZERO;

    public abstract Integer getBaseDamage();

    public abstract Integer getDamageVariance();

    public Integer rollDamage() {
        Integer result = getBaseDamage() + Random.getInteger(-getDamageVariance(), getDamageVariance());
        if (result < 0) {
            result = 0;
        }

        return result;
    }

    public abstract TimeDuration getBaseCooldownTime();

    public abstract Integer getMaxRange();

    public boolean isGun() {
        return false;
    }

    public boolean isMelee() {
        return false;
    }

    public boolean canUse(TimePoint now) {
        return !lastUsed.durationTo(now).shorterThan(getBaseCooldownTime());
    }

    public void use(TimePoint timePoint) {
        lastUsed = timePoint.copy();
    }

    public abstract String classString();

    public abstract String useSound();

}
