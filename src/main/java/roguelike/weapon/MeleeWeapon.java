package roguelike.weapon;

public abstract class MeleeWeapon extends Weapon {

    @Override
    public boolean isMelee() {
        return true;
    }

    @Override
    public String classString() {
        return "Melee";
    }
}
