package roguelike.weapon;

import roguelike.core.time.TimeDuration;

public abstract class Gun extends Weapon {
    Integer ammo = 0;

    @Override
    public boolean isGun() {
        return true;
    }

    public abstract Double getInaccuracy();

    public abstract Integer getMaxAmmo();

    public Integer getAmmo() {
        return ammo;
    }

    public abstract TimeDuration getBaseReloadTime();

    public void reload() {
        ammo = getMaxAmmo();
    }

    public void decreaseAmmo() {
        if (ammo > 0) {
            --ammo;
        }
    }

    @Override
    public String classString() {
        return "Gun";
    }


}
