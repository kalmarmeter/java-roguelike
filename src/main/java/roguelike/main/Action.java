package roguelike.main;

import roguelike.core.Vector2i;
import roguelike.core.time.TimeDuration;
import roguelike.core.time.TimePoint;
import roguelike.entity.Entity;

import java.util.Comparator;

public abstract class Action {
    ActionType type;
    Entity actor = null;
    Entity targetEntity = null;
    Vector2i target = null;
    TimeDuration remaining;

    protected Action(ActionType type, Entity actor, Entity targetEntity, TimeDuration duration) {
        this.type = type;
        this.actor = actor;
        this.targetEntity = targetEntity;
        this.remaining = duration;
    }

    protected Action(ActionType type, Entity actor, Vector2i target, TimeDuration duration) {
        this.type = type;
        this.actor = actor;
        this.target = target;
        this.remaining = duration;
    }

    public ActionType getType() {
        return type;
    }

    public Entity getActor() {
        return actor;
    }

    public Entity getTargetEntity() {
        return targetEntity;
    }

    public Vector2i getTarget() {
        return target;
    }

    public TimeDuration getRemaining() {
        return remaining;
    }

    public void subtractDuration(TimeDuration duration) {
        this.remaining = this.remaining.subtract(duration);
    }

    public abstract void execute(Game game);

}

class ActionComparator implements Comparator<Action> {
    public int compare(Action lhs, Action rhs) {
        if (lhs.getRemaining().shorterThan(rhs.getRemaining())) {
            return -1;
        } else {
            return 1;
        }
    }
}
