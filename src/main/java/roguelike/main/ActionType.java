package roguelike.main;

public enum ActionType {
    //Nothing
    DO_NOTHING,

    //Waiting
    WAIT,

    //Movement
    MOVE,
    STANCE,

    //Combat
    WEAPON,
    EXPLODE,
    RELOAD
}
