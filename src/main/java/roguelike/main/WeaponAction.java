package roguelike.main;

import roguelike.core.Vector2i;
import roguelike.core.time.TimeDuration;
import roguelike.entity.Entity;
import roguelike.weapon.Weapon;

public class WeaponAction extends Action {

    public WeaponAction(Entity actor, Vector2i target, TimeDuration duration) {
        super(ActionType.WEAPON, actor, target, duration);
    }

    public WeaponAction(Entity actor, Entity targetEntity, TimeDuration duration) {
        super(ActionType.WEAPON, actor, targetEntity, duration);
    }

    public void execute(Game game) {
        Weapon weapon = actor.getCurrentWeapon();
        weapon.use(game.getTimePoint());
        if (getTargetEntity() == null) {
            if (weapon.isGun()) {
                game.shoot(actor, getTarget());
            } else if (weapon.isMelee()) {
                game.hit(actor, getTarget());
            }
        } else {
            if (weapon.isGun()) {
                game.shoot(actor, getTargetEntity().getPosition());
            } else if (weapon.isMelee()) {
                game.hit(actor, getTargetEntity().getPosition());
            }
        }
    }

    @Override
    public String toString() {
        return "Using weapon";
    }
}
