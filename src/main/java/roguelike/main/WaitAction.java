package roguelike.main;

import roguelike.core.time.TimeDuration;
import roguelike.entity.Entity;

public class WaitAction extends Action {

    public WaitAction(Entity actor, TimeDuration duration) {
        super(ActionType.DO_NOTHING, actor, (Entity)null, duration);
    }

    public void execute(Game game) {

    }

    @Override
    public String toString() {
        return "Waiting";
    }
}
