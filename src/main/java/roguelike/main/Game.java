package roguelike.main;

import com.googlecode.lanterna.input.KeyStroke;
import roguelike.ai.AI;
import roguelike.core.*;
import roguelike.core.Random;
import roguelike.core.time.TimeDuration;
import roguelike.core.time.TimePoint;
import roguelike.entity.Attributes;
import roguelike.entity.Entity;
import roguelike.entity.Stance;
import roguelike.entity.entities.TestEnemy;
import roguelike.entity.entities.TestRocket;
import roguelike.graphics.game.AnimationAttributes;
import roguelike.graphics.game.Camera;
import roguelike.graphics.Window;
import roguelike.graphics.game.animations.ExplosionAnimation;
import roguelike.graphics.game.animations.ShootAnimation;
import roguelike.map.Chunk;
import roguelike.map.Map;
import roguelike.map.Tile;
import roguelike.map.TileProperties;
import roguelike.player.Player;
import roguelike.ray.BloodTracer;
import roguelike.ray.VisionRayTracer;
import roguelike.sound.SoundManager;
import roguelike.weapon.Gun;
import roguelike.weapon.MeleeWeapon;
import roguelike.weapon.Weapon;

import java.io.IOException;
import java.util.*;

public class Game {
    Camera camera;
    Window window;
    Map map;
    List<Entity> entities;
    Player player;
    Boolean isRunning;
    Boolean playerAction;
    TimePoint timePoint;
    long lastFrame = 0;
    long lastKeyPress = 0;
    PriorityQueue<Action> actionQueue;
    GameState gameState;
    Vector2i selectedTile = null;
    Entity lastShotEntity = null;
    List<String> messageLog;
    BloodTracer bloodTracer;

    public Game() {

    }

    public Camera getCamera() {
        return camera;
    }

    public Map getMap() {
        return map;
    }

    public List<Entity> getEntities() {
        return entities;
    }

    public Player getPlayer() {
        return player;
    }

    public TimePoint getTimePoint() {
        return timePoint;
    }

    public void run() {
        try {
            initialize();
            lastFrame = System.currentTimeMillis();
            while (isRunning) {
                if (lastFrame - lastKeyPress > 100) {
                    processKeyboardInput();
                }
                if (isRunning) {
                    update();
                    draw();

                    long currentFrame = System.currentTimeMillis();
                    long frameTime = currentFrame - lastFrame;

                    //Locked 60 fps for now
                    if (frameTime < 16) {
                        Thread.sleep(16 - frameTime);
                    }

                    lastFrame = System.currentTimeMillis();
                }

            }
            cleanup();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void draw() throws IOException {
        window.clear();
        window.draw();
        window.display();
    }

    private void update() {
        if (playerAction) {
            updateEntities();
            processActionQueue();
            playerAction = false;
        }
    }

    private void updateEntities() {
        Iterator<Entity> iterator = entities.iterator();
        while (iterator.hasNext()) {
            Entity entity = iterator.next();
            if (entity.isDead()) {
                Chunk chunk = map.getChunkAt(entity.getPosition());
                chunk.removeEntity(entity);
                iterator.remove();
                continue;
            }

            entity.updateVision();

            if (!entity.hasAction()) {
                actionQueue.add(entity.nextAction());
            }
        }
    }

    private void updateTiles() {
        map.updateTiles();
    }

    private void processKeyboardInput() throws IOException {
        KeyStroke key;
        while ((key = window.getTerminal().pollInput()) != null) {
            switch (key.getKeyType()) {
                case Character:
                    Character ch = key.getCharacter();
                    switch (ch) {
                        case 'w':
                            if (gameState == GameState.CONTROLLING_PLAYER) {
                                if (!map.isSolidTile(player.getPosition().add(Vector2i.UP))) {
                                    actionQueue.add(new MoveAction(player, player.getPosition().add(Vector2i.UP),
                                            getMovementCost(player, player.getPosition().add(Vector2i.UP))));
                                    playerAction = true;
                                }
                            } else {
                                selectedTile = selectedTile.add(Vector2i.UP);
                            }
                            break;
                        case 'a':
                            if (gameState == GameState.CONTROLLING_PLAYER) {
                                if (!map.isSolidTile(player.getPosition().add(Vector2i.LEFT))) {
                                    actionQueue.add(new MoveAction(player, player.getPosition().add(Vector2i.LEFT),
                                            getMovementCost(player, player.getPosition().add(Vector2i.LEFT))));
                                    playerAction = true;
                                }
                            } else {
                                selectedTile = selectedTile.add(Vector2i.LEFT);
                            }
                            break;
                        case 's':
                            if (gameState == GameState.CONTROLLING_PLAYER) {
                                if (!map.isSolidTile(player.getPosition().add(Vector2i.DOWN))) {
                                    actionQueue.add(new MoveAction(player, player.getPosition().add(Vector2i.DOWN),
                                            getMovementCost(player, player.getPosition().add(Vector2i.DOWN))));
                                    playerAction = true;
                                }
                            } else {
                                selectedTile = selectedTile.add(Vector2i.DOWN);
                            }
                            break;
                        case 'd':
                            if (gameState == GameState.CONTROLLING_PLAYER) {
                                if (!map.isSolidTile(player.getPosition().add(Vector2i.RIGHT))) {
                                    actionQueue.add(new MoveAction(player, player.getPosition().add(Vector2i.RIGHT),
                                            getMovementCost(player, player.getPosition().add(Vector2i.RIGHT))));
                                    playerAction = true;
                                }
                            } else {
                                selectedTile = selectedTile.add(Vector2i.RIGHT);
                            }
                            break;
                        case ' ':
                            if (gameState == GameState.CONTROLLING_PLAYER) {
                                actionQueue.add(new WaitAction(player, TimeDuration.seconds(1)));
                                playerAction = true;
                            }
                            break;
                        case 'k':
                            if (gameState == GameState.CONTROLLING_PLAYER) {
                                selectedTile = player.getPosition();
                                gameState = GameState.SELECTING_TILE;
                            } else if (gameState == GameState.SELECTING_TILE) {
                                selectedTile = null;
                                gameState = GameState.CONTROLLING_PLAYER;
                            }
                            break;
                        case 'e':
                            if (gameState == GameState.SELECTING_TILE) {
                                addEntity(new TestRocket(player.getPosition(), selectedTile));
                            }
                            break;
                        case 'f':
                            if (gameState == GameState.SELECTING_TILE) {
                                if (player.canUseWeapon(timePoint)) {
                                    actionQueue.add(new WeaponAction(player, selectedTile, TimeDuration.seconds(1)));
                                    selectedTile = null;
                                    gameState = GameState.CONTROLLING_PLAYER;
                                    playerAction = true;
                                }
                            }
                            break;
                        case 'g':
                            if (gameState == GameState.CONTROLLING_PLAYER) {
                                if (lastShotEntity != null &&  !lastShotEntity.isDead() && player.isPositionVisible(lastShotEntity.getPosition())) {
                                    actionQueue.add(new WeaponAction(player, lastShotEntity, TimeDuration.seconds(1)));
                                    playerAction = true;
                                }
                            }
                            break;
                        case 'c':
                            if (gameState == GameState.CONTROLLING_PLAYER) {
                                if (player.getStance() == Stance.CROUCHING) {
                                    actionQueue.add(new StanceAction(player, Stance.STANDING, TimeDuration.seconds(2)));
                                } else {
                                    actionQueue.add(new StanceAction(player, Stance.CROUCHING, TimeDuration.seconds(2)));
                                }
                                playerAction = true;
                            }
                            break;
                        case 'r':
                            if (gameState == GameState.CONTROLLING_PLAYER) {
                                actionQueue.add(new ReloadAction(player, ((Gun)player.getCurrentWeapon()).getBaseReloadTime()));
                                playerAction = true;
                            }
                            break;
                        default:
                            break;
                    }

                    break;
                case Escape:
                    isRunning = false;
                    return;
                default:
                    break;
            }

            lastKeyPress = System.currentTimeMillis();

            if (playerAction) {
                while (window.getTerminal().pollInput() != null) {}
                break;
            }
        }
    }

    private void processActionQueue() {
        //Sort the actions in the list based on time remaining
        while (true) {

            Action action = actionQueue.poll();
            if (action == null) {
                break;
            }
            Entity actor = action.getActor();
            if (!actor.isDead()) {
                TimeDuration timeTaken = action.getRemaining();
                action.execute(this);

                for (Action otherAction : actionQueue) {
                    otherAction.subtractDuration(timeTaken);
                }

                advanceTime(timeTaken);

                if (actor == player) {
                    player.updateVision();
                    break;
                }

                if (player.isDead()) {
                    isRunning = false;
                    break;
                }

                actor.removeAction();

                if (!actor.isDead()) {
                    actionQueue.add(actor.nextAction());
                }

            }

        }

    }

    private void addEntity(Entity entity) {
        entities.add(entity);
        Chunk chunk = map.getChunkAt(entity.getPosition());
        chunk.addEntity(entity);
    }

    private void spawnEntities() {
        for (int i = 0; i < 10; ++i) {
            Entity newEntity = new TestEnemy(new Vector2i(Random.getInteger(map.WIDTH), Random.getInteger(map.HEIGHT)), player);
            newEntity.setVisionRayTracer(new VisionRayTracer(newEntity, map, 10));
            addEntity(newEntity);
        }
    }

    private Entity getEntityAt(Vector2i position) {
        return map.entityAt(position);
    }

    void explode(Vector2i position) {
        addMessage("Explosion happens at " + position);
        SoundManager.play("explosion", 30);
        window.getGameRenderer().addAnimation(new ExplosionAnimation(10), new AnimationAttributes(position, 1));
        map.explode(position, 4);
        Rectanglei area = new Rectanglei(position.subtract(new Vector2i(4, 4)), new Vector2i(4 * 2, 4 * 2));
        for (Vector2i pos : area) {
            Entity entity = getEntityAt(pos);
            if (entity != null) {
                if (pos.distanceTo(position) <= 4) {
                    Integer damage = Random.getInteger(25, 50);
                    entity.hurt(damage);
                }
            }
        }
    }

    void shoot(Entity shooter, Vector2i target) {

        Gun gun = (Gun)shooter.getCurrentWeapon();
        if (gun.getAmmo() == 0) {
            SoundManager.play("empty", 30);
            return;
        }

        SoundManager.play(gun.useSound(), 30);

        Entity targetedEntity = getEntityAt(target);
        if (shooter == player && targetedEntity != null && lastShotEntity != targetedEntity) {
            lastShotEntity = targetedEntity;
        }

        //addMessage(shooter + " shoots");

        double angle = target.subtract(shooter.getPosition()).getAngle();
        double realAngle = angle + Random.getDouble(CommonMath.fromDegrees(-gun.getInaccuracy()), CommonMath.fromDegrees(+gun.getInaccuracy()));

        Vector2i realTarget = shooter.getPosition().add(Vector2i.fromLengthAndAngle(target.subtract(shooter.getPosition()).length(), realAngle));

        List<Vector2i> trajectory = Bresenham.line(shooter.getPosition(), realTarget);
        Vector2i endPoint = null;
        if (trajectory.size() >= 1) {
            trajectory.remove(0);
        }
        for (Vector2i point : trajectory) {
            endPoint = point;
            Tile tile = map.getTile(point);
            if (tile == null) {
                break;
            }
            if (TileProperties.getTileProperty(tile).isSolid()) {
                break;
            }
            Entity entity = getEntityAt(point);
            if (entity != null && !entity.isDead()) {
                addMessage(entity + " is hit by a " + gun);
                entity.hurt(gun.rollDamage());
                bloodTracer.splatterBlood(entity.getPosition(), realAngle);
            }
        }

        gun.decreaseAmmo();

        if (endPoint != null) {
            List<Pair<ShootAnimation, Vector2i>> animationList = ShootAnimation.getAnimation(shooter.getPosition(), endPoint);
            for (Pair<ShootAnimation, Vector2i> pair : animationList) {
                window.getGameRenderer().addAnimation(pair.getFirst(), new AnimationAttributes(pair.getSecond(), 1, false));
            }
        }
    }

    protected void hit(Entity attacker, Vector2i target) {
        MeleeWeapon weapon = (MeleeWeapon)attacker.getCurrentWeapon();

        SoundManager.play(weapon.useSound(), 30);

        Entity targetedEntity = getEntityAt(target);
        if (attacker == player && targetedEntity != lastShotEntity && targetedEntity != null) {
            lastShotEntity = targetedEntity;
        }

        if (targetedEntity != null && !targetedEntity.isDead()) {
            targetedEntity.hurt(weapon.rollDamage());
            bloodTracer.splatterBlood(targetedEntity.getPosition(), target.subtract(attacker.getPosition()).getAngle());
            addMessage(targetedEntity + " is hit by a " + weapon);
        }
    }

    private void initialize() throws IOException {

        SoundManager.add("ar_shoot", "resources/sfx/ar_shoot_01.wav");
        SoundManager.add("ar_shoot", "resources/sfx/ar_shoot_02.wav");
        SoundManager.add("ar_shoot", "resources/sfx/ar_shoot_03.wav");
        SoundManager.add("ar_shoot", "resources/sfx/ar_shoot_04.wav");
        SoundManager.add("bolt_shoot", "resources/sfx/bolt_shoot_01.wav");
        SoundManager.add("knife_slash", "resources/sfx/knife_slash_01.wav");
        SoundManager.add("explosion", "resources/sfx/explosion_01.wav");
        SoundManager.add("empty", "resources/sfx/ar_empty_01.wav");
        SoundManager.add("hurt", "resources/sfx/hit_01.wav");
        SoundManager.add("hurt", "resources/sfx/hit_02.wav");
        SoundManager.add("hurt", "resources/sfx/hit_03.wav");
        SoundManager.add("hurt", "resources/sfx/hit_04.wav");
        SoundManager.add("guts", "resources/sfx/guts_01.wav");
        SoundManager.add("reload", "resources/sfx/ar_reload_01.wav");
        SoundManager.add("step", "resources/sfx/grass_step_01.wav");
        //SoundManager.add("step", "resources/sfx/grass_step_02.wav");
        SoundManager.add("step", "resources/sfx/grass_step_03.wav");
        //SoundManager.add("step", "resources/sfx/grass_step_04.wav");

        actionQueue = new PriorityQueue<Action>(20, new ActionComparator());

        timePoint = TimePoint.ZERO.copy();

        camera = new Camera();

        player = new Player(new Vector2i(4, 4));

        window = new Window();
        window.initialize(this);

        map = new Map(this);
        map.generate();

        player.setVisionRayTracer(new VisionRayTracer(player, map, 14));
        Chunk chunk = map.getChunkAt(player.getPosition());
        chunk.addEntity(player);

        AI.game = this;

        camera.setEntityFollowing(player);
        camera.setBounds(map.getMapArea());

        entities = new ArrayList<Entity>();
        spawnEntities();

        isRunning = true;
        playerAction = false;

        gameState = GameState.CONTROLLING_PLAYER;
        player.updateVision();

        messageLog = new ArrayList<String>();

        bloodTracer = new BloodTracer(map);
    }

    public void addMessage(String message) {
        messageLog.add(timePoint.toString() + " " + message);
    }

    public List<String> getMessageLog() {
        return messageLog;
    }

    private void cleanup() throws IOException {
        window.cleanup();
    }

    private void clearActionQueue() {
        actionQueue.clear();
    }

    boolean tryMoveTo(Entity entity, Vector2i destination) {
        Tile destinationTile = map.getTile(destination);
        //Out of bounds destination
        if (destinationTile == null) {
            return false;
        }

        if (!TileProperties.getTileProperty(destinationTile).isSolid() && map.entityAt(destination) == null) {
            Chunk oldChunk = map.getChunkAt(entity.getPosition());
            entity.setPosition(destination);
            Chunk newChunk = map.getChunkAt(entity.getPosition());
            if (oldChunk != newChunk) {
                oldChunk.removeEntity(entity);
                newChunk.addEntity(entity);
            }

            if (entity == player) {
                SoundManager.play("step", 7);
            }
            return true;
        }

        return false;
    }

    public TimeDuration getMovementCost(Entity entity, Vector2i target) {
        Tile targetTile = map.getTile(target);
        if (targetTile == null) {
            return null;
        }

        Attributes attributes = entity.getAttributes();
        TileProperties targetTileProperties = TileProperties.getTileProperty(targetTile);

        TimeDuration movementCost = TimeDuration.seconds(targetTileProperties.getBaseMoveCost());
        movementCost = movementCost.multiply(1.0f / attributes.getAgility());
        if (entity.getStance() == Stance.CROUCHING) {
            movementCost = movementCost.multiply(2.0f);
        }

        return movementCost;
    }

    private void advanceTime(TimeDuration elapsedTime) {
        TimePoint endTime = timePoint.add(elapsedTime);
        while (timePoint.before(endTime)) {
            timePoint.advance(TimeDuration.SECOND);
            if (timePoint.every(TimeDuration.seconds(5))) {
                updateEntities();
            }
            if (timePoint.every(TimeDuration.seconds(10))) {
                updateTiles();
            }
        }
    }

    public Window getWindow() {
        return window;
    }

    public GameState getGameState() {
        return gameState;
    }

    public Vector2i getSelectedTile() {
        return selectedTile;
    }
}
