package roguelike.main;

import roguelike.core.Vector2i;
import roguelike.core.time.TimeDuration;
import roguelike.entity.Entity;
import roguelike.entity.Stance;

public class StanceAction extends Action {
    Stance stance;

    public StanceAction(Entity actor, Stance stance, TimeDuration duration) {
        super(ActionType.STANCE, actor, (Vector2i)null, duration);
        this.stance = stance;
    }

    public void execute(Game game) {
        if (stance == Stance.CROUCHING) {
            actor.crouch();
        } else if (stance == Stance.STANDING) {
            actor.stand();
        }
    }

    @Override
    public String toString() {
        return "Changing stance";
    }
}
