package roguelike.main;

import roguelike.core.Vector2i;
import roguelike.core.time.TimeDuration;
import roguelike.core.time.TimePoint;
import roguelike.entity.Entity;

public class MoveAction extends Action {

    public MoveAction(Entity actor, Vector2i target, TimeDuration duration) {
        super(ActionType.MOVE, actor, target, duration);
    }

    public void execute(Game game) {
        game.tryMoveTo(actor, target);
    }

    @Override
    public String toString() {
        return "Moving";
    }
}
