package roguelike.main;

import roguelike.core.Vector2i;
import roguelike.core.time.TimeDuration;
import roguelike.entity.Entity;
import roguelike.sound.SoundManager;
import roguelike.weapon.Gun;

public class ReloadAction extends Action {

    public ReloadAction(Entity actor, TimeDuration duration) {
        super(ActionType.RELOAD, actor, (Vector2i)null, duration);
    }

    public void execute(Game game) {
        SoundManager.play("reload", 30);
        ((Gun)actor.getCurrentWeapon()).reload();
    }

    @Override
    public String toString() {
        return "Reloading";
    }
}
