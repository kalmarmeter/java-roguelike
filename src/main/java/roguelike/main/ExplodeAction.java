package roguelike.main;

import roguelike.core.Vector2i;
import roguelike.core.time.TimeDuration;
import roguelike.entity.Entity;

public class ExplodeAction extends Action {

    boolean actorDies;

    public ExplodeAction(Entity actor, boolean actorDies, TimeDuration duration) {
        super(ActionType.EXPLODE, actor, (Vector2i)null, duration);
        this.actorDies = actorDies;
    }

    public void execute(Game game) {
        game.explode(actor.getPosition());
        if (actorDies) {
            actor.die();
        }
    }

    @Override
    public String toString() {
        return "Exploding";
    }
}
