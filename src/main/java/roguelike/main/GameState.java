package roguelike.main;

public enum GameState {
    CONTROLLING_PLAYER,
    SELECTING_TILE
}
