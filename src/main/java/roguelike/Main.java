package roguelike;

import roguelike.main.Game;

public class Main {
    public static void main(String... args) {
        Game game = new Game();
        game.run();
    }
}
