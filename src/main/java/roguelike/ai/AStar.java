package roguelike.ai;

import roguelike.core.Vector2i;

import java.util.*;

public abstract class AStar {

    static class Node implements Comparable<Node> {
        Vector2i position;
        double fScore = Double.POSITIVE_INFINITY;
        double gScore = Double.POSITIVE_INFINITY;
        Node previous = null;

        Node(Vector2i position) {
            this.position = position;
        }

        @Override
        public boolean equals(Object other) {
            if (other == null) {
                return false;
            }

            if (other instanceof Node) {
                return position.equals(((Node)other).position);
            }

            return false;
        }

        public int compareTo(Node o) {
            if (equals(o)) return 0;
            if (fScore < o.fScore) {
                return -1;
            } else {
                return 1;
            }
        }
    }

    protected abstract boolean isValidPosition(Vector2i position);
    protected abstract double getMovementCost(Vector2i target);

    protected List<Node> findPath(Vector2i start, Vector2i target) {
        Map<Vector2i, Node> nodes = new HashMap<Vector2i, Node>();
        PriorityQueue<Node> openSet = new PriorityQueue<Node>();
        Node startNode = new Node(start);
        startNode.gScore = 0.0;
        startNode.fScore = target.subtract(start).manhattanLength();
        openSet.add(startNode);
        nodes.put(start, startNode);

        while (!openSet.isEmpty()) {
            Node current = openSet.poll();
            if (current.position.equals(target)) {
                return reconstructPath(current);
            }

            for (Vector2i neighborPos : current.position.getNeighbours()) {
                if (isValidPosition(neighborPos)) {
                    Node neighbor = nodes.get(neighborPos);
                    if (neighbor == null) {
                        neighbor = new Node(neighborPos);
                        nodes.put(neighborPos, neighbor);
                    }
                    double tentativeGScore = current.gScore + getMovementCost(neighborPos);
                    if (tentativeGScore < neighbor.gScore) {
                        neighbor.previous = current;
                        neighbor.gScore = tentativeGScore;
                        neighbor.fScore = neighbor.gScore + target.subtract(neighbor.position).manhattanLength();
                        if (!openSet.contains(neighbor)) {
                            openSet.add(neighbor);
                        }
                    }
                }
            }
        }

        return null;
    }

    protected List<Node> reconstructPath(Node end) {
        List<Node> pathNodes = new ArrayList<Node>();
        Node current = end;
        while (current != null) {
            pathNodes.add(current);
            current = current.previous;
        }

        Collections.reverse(pathNodes);

        return pathNodes;
    }
}
