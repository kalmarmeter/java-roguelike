/*
package roguelike.ai.ais;

import roguelike.main.Action;
import roguelike.main.ActionType;
import roguelike.ai.AI;
import roguelike.core.time.TimeDuration;
import roguelike.core.time.TimePoint;
import roguelike.core.Vector2i;
import roguelike.entity.Entity;

public class SimpleMoveTowardsAI extends AI {

    Entity actor;
    Vector2i target;
    Entity targetEntity = null;

    public SimpleMoveTowardsAI(Entity actor, Vector2i target) {
        this.actor = actor;
        this.target = target;
    }

    public SimpleMoveTowardsAI(Entity actor, Entity target) {
        this.actor = actor;
        this.targetEntity = target;
        //this.target = targetEntity.getPosition();
    }

    public Action nextAction() {
        if (targetEntity != null && actor.isPositionVisible(targetEntity.getPosition())) {
            this.target = targetEntity.getPosition();
        }

        int distanceX = target.getX() - actor.getPosition().getX();
        int distanceY = target.getY() - actor.getPosition().getY();

        if (distanceX == 0 && distanceY == 0) {
            return new Action(ActionType.DO_NOTHING, actor, (Vector2i)null, TimePoint.ZERO, TimeDuration.seconds(1));
        }

        boolean verticalMove = false;
        if ((distanceX == 0 || Math.abs(distanceX) > Math.abs(distanceY)) && distanceY != 0) {
            verticalMove = true;
        }

        if (!verticalMove) {
            if (distanceX > 0) {
                return new Action(ActionType.MOVE_RIGHT, actor, (Vector2i)null, TimePoint.ZERO,
                        game.getMovementCost(actor, actor.getPosition().add(Vector2i.RIGHT)));
            } else {
                return new Action(ActionType.MOVE_LEFT, actor, (Vector2i)null, TimePoint.ZERO,
                        game.getMovementCost(actor, actor.getPosition().add(Vector2i.LEFT)));
            }
        } else {
            if (distanceY > 0) {
                return new Action(ActionType.MOVE_DOWN, actor, (Vector2i)null, TimePoint.ZERO,
                        game.getMovementCost(actor, actor.getPosition().add(Vector2i.DOWN)));
            } else {
                return new Action(ActionType.MOVE_UP, actor, (Vector2i)null, TimePoint.ZERO,
                        game.getMovementCost(actor, actor.getPosition().add(Vector2i.UP)));
            }
        }
    }

}
*/
