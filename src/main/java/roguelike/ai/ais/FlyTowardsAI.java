package roguelike.ai.ais;

import roguelike.main.*;
import roguelike.ai.AI;
import roguelike.core.Bresenham;
import roguelike.core.Vector2i;
import roguelike.core.time.TimeDuration;
import roguelike.core.time.TimePoint;
import roguelike.entity.Entity;
import roguelike.map.TileHeight;
import roguelike.map.TileProperties;

import java.util.List;

public class FlyTowardsAI extends AI {
    Entity actor;
    List<Vector2i> path;

    public FlyTowardsAI(Entity actor, Vector2i target) {
        path = Bresenham.line(actor.getPosition(), target);
        this.actor = actor;
    }

    public Action nextAction() {
        //Meh, now movement always goes every second
        if (path.isEmpty() || TileProperties.getTileProperty(game.getMap().getTile(actor.getPosition())).getHeight() == TileHeight.FULL) {
            return new ExplodeAction(actor, true, TimeDuration.seconds(1));
        }

        Vector2i nextPosition = path.remove(0);
        Vector2i direction = nextPosition.subtract(actor.getPosition());
        Action action = new WaitAction(actor, TimeDuration.seconds(1));
        if (direction.equals(Vector2i.UP)) {
            action = new MoveAction(actor, actor.getPosition().add(Vector2i.UP),
                    TimeDuration.seconds(1));
        } else if (direction.equals(Vector2i.LEFT)) {
            action = new MoveAction(actor, actor.getPosition().add(Vector2i.LEFT),
                    TimeDuration.seconds(1));
        } else if (direction.equals(Vector2i.DOWN)) {
            action = new MoveAction(actor, actor.getPosition().add(Vector2i.DOWN),
                    TimeDuration.seconds(1));
        } else if (direction.equals(Vector2i.RIGHT)) {
            action = new MoveAction(actor, actor.getPosition().add(Vector2i.RIGHT),
                    TimeDuration.seconds(1));
        } else {
            actor.setPosition(nextPosition);
        }

        return action;
    }
}
