package roguelike.ai.ais;

import roguelike.core.Random;
import roguelike.main.*;
import roguelike.ai.AI;
import roguelike.ai.BaseEntityAStar;
import roguelike.core.Vector2i;
import roguelike.core.time.TimeDuration;
import roguelike.core.time.TimePoint;
import roguelike.entity.Entity;
import roguelike.weapon.Gun;
import roguelike.weapon.Weapon;

import java.util.ArrayList;
import java.util.List;

public class AStarMoveTowardsAI extends AI {
    BaseEntityAStar aStar;
    Entity actor;
    Entity targetEntity;
    Vector2i target;
    List<Vector2i> currentPath;

    public AStarMoveTowardsAI(Entity actor, Entity targetEntity) {
        this.actor = actor;
        this.targetEntity = targetEntity;
        this.target = null;
        currentPath = new ArrayList<Vector2i>();
        aStar = new BaseEntityAStar(actor, AI.game);
    }

    public AStarMoveTowardsAI(Entity actor, Vector2i target) {
        this.actor = actor;
        this.target = target;
        currentPath = new ArrayList<Vector2i>();
        aStar = new BaseEntityAStar(actor, AI.game);
    }

    public Action nextAction() {
        if (targetEntity != null) {
            if (actor.isPositionVisible(targetEntity.getPosition())) {
                if (target == null || !target.equals(targetEntity.getPosition())) {
                    target = targetEntity.getPosition();

                    if (currentPath != null) {
                        currentPath.clear();
                    }

                    currentPath = aStar.getPath(target);
                }
            }
        }

        Weapon weapon = actor.getCurrentWeapon();
        double targetDistance = actor.getPosition().distanceTo(targetEntity.getPosition());
        boolean inRange = targetDistance <= weapon.getMaxRange();

        if (weapon.isGun() && ((Gun)weapon).getAmmo() == 0) {
            return new ReloadAction(actor, ((Gun)weapon).getBaseReloadTime());
        }

        if (actor.isPositionVisible(targetEntity.getPosition()) &&
                actor.getCurrentWeapon().canUse(AI.game.getTimePoint()) &&
                inRange
        ) {
            return new WeaponAction(actor, targetEntity, TimeDuration.seconds(1));
        }

        if (target == null || currentPath == null || currentPath.size() == 0) {
            return new WaitAction(actor, TimeDuration.seconds(3));
        }

        Action action;
        if (!inRange) {
            Vector2i nextDirection = currentPath.remove(0);

            if (nextDirection.equals(Vector2i.UP)) {
                action = new MoveAction(actor, actor.getPosition().add(Vector2i.UP),
                        AI.game.getMovementCost(actor, actor.getPosition().add(Vector2i.UP)));
            } else if (nextDirection.equals(Vector2i.LEFT)) {
                action = new MoveAction(actor, actor.getPosition().add(Vector2i.LEFT),
                        AI.game.getMovementCost(actor, actor.getPosition().add(Vector2i.LEFT)));
            } else if (nextDirection.equals(Vector2i.DOWN)) {
                action = new MoveAction(actor, actor.getPosition().add(Vector2i.DOWN),
                        AI.game.getMovementCost(actor, actor.getPosition().add(Vector2i.DOWN)));
            } else if (nextDirection.equals(Vector2i.RIGHT)) {
                action = new MoveAction(actor, actor.getPosition().add(Vector2i.RIGHT),
                        AI.game.getMovementCost(actor, actor.getPosition().add(Vector2i.RIGHT)));
            } else {
                action = new WaitAction(actor, TimeDuration.seconds(5));
            }
        } else {
            return new WaitAction(actor, TimeDuration.seconds(1));
        }

        return action;

    }
}
