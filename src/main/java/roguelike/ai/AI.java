package roguelike.ai;

import roguelike.main.Action;
import roguelike.main.Game;

public abstract class AI {
    public static Game game;

    public AI() {

    }

    public abstract Action nextAction();
}
