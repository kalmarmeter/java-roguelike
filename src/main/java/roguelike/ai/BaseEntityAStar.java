package roguelike.ai;

import roguelike.main.Game;
import roguelike.core.Vector2i;
import roguelike.entity.Entity;

import java.util.ArrayList;
import java.util.List;

public class BaseEntityAStar extends AStar {

    Entity entity;
    Game game;

    public BaseEntityAStar(Entity entity, Game game) {
        this.entity = entity;
        this.game = game;
    }

    protected boolean isValidPosition(Vector2i position) {
        return game.getMap().getMapArea().contains(position) && !game.getMap().isSolidTile(position);
    }

    protected double getMovementCost(Vector2i target) {
        return game.getMovementCost(entity, target).asSeconds();
    }

    public List<Vector2i> getPath(Vector2i target) {
        List<Node> pathNodes = findPath(entity.getPosition(), target);
        if (pathNodes == null) {
            return null;
        }

        List<Vector2i> pathDirections = new ArrayList<Vector2i>();
        for (int i = 0; i < pathNodes.size() - 1; ++i) {
            Node current = pathNodes.get(i);
            Node next = pathNodes.get(i + 1);

            Vector2i direction = next.position.subtract(current.position);

            pathDirections.add(direction);
        }

        return pathDirections;

    }
}
