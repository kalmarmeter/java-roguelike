package roguelike.core;

import java.util.Iterator;

public class Rectanglei implements Iterable<Vector2i> {
    Vector2i position;
    Vector2i size;

    public Rectanglei() {
        position = new Vector2i();
        size = new Vector2i();
    }

    public Rectanglei(Vector2i position, Vector2i size) {
        this.position = position;
        this.size = size;
    }

    public Vector2i getPosition() {
        return position;
    }

    public void setPosition(Vector2i position) {
        this.position = position;
    }

    public Vector2i getSize() {
        return size;
    }

    public Integer getWidth() {
        return size.getX();
    }

    public Integer getHeight() {
        return size.getY();
    }

    public Vector2i topLeft() {
        return this.position;
    }

    public Vector2i topRight() {
        return new Vector2i(right(), top());
    }

    public Vector2i bottomLeft() {
        return new Vector2i(left(), bottom());
    }

    public Vector2i bottomRight() {
        return new Vector2i(right(), bottom());
    }

    public Integer left() {
        return this.position.getX();
    }

    public Integer top() {
        return this.position.getY();
    }

    public Integer right() {
        return this.position.getX() + this.size.getX() - 1;
    }

    public Integer bottom() {
        return this.position.getY() + this.size.getY() - 1;
    }

    public void setSize(Vector2i size) {
        if (size.x > 0 && size.y > 0) {
            this.size = size;
        }
    }

    public Boolean contains(Vector2i point) {
        return CommonMath.isInsideRange(point.getX(), left(), right()) &&
                CommonMath.isInsideRange(point.getY(), top(), bottom());
    }

    public Vector2i getRelativePosition(Vector2i point) {
        return point.subtract(position);
    }

    public Vector2i getGlobalPosition(Vector2i relativePoint) {
        return relativePoint.add(position);
    }

    public Vector2i clamp(Vector2i point) {
        return new Vector2i(
                CommonMath.clamp(point.getX(), left(), right()),
                CommonMath.clamp(point.getY(), top(), bottom()));
    }

    public Iterator<Vector2i> iterator() {
        return new RectangleiIterator(this);
    }
}

class RectangleiIterator implements Iterator<Vector2i> {
    Rectanglei rectangle;
    Vector2i position;

    public RectangleiIterator(Rectanglei rectangle) {
        this.rectangle = rectangle;
        position = rectangle.getPosition().subtract(Vector2i.RIGHT);
    }

    public boolean hasNext() {
        return (position.getY() < rectangle.bottom() ||
                (position.getY() == rectangle.bottom() && position.getX() < rectangle.right()));
    }

    public Vector2i next() {
        if (position.getX() < rectangle.right()) {
            position = position.add(Vector2i.RIGHT);
        } else {
            position = new Vector2i(rectangle.left(), position.getY() + 1);
        }

        return position;
    }

    public void remove() {
        //Nothing
    }
}
