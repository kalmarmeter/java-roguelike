package roguelike.core;

import java.util.List;

public class Random {
    static java.util.Random random = new java.util.Random();

    public static void setSeed(Long seed) {
        random.setSeed(seed);
    }

    public static Integer getInteger() {
        return random.nextInt();
    }

    public static Integer getInteger(Integer max) {
        return random.nextInt(max);
    }

    public static Integer getInteger(Integer min, Integer max) {
        return min + random.nextInt(max - min);
    }

    public static double getDouble() {
        return random.nextDouble();
    }

    public static double getDouble(double max) {
        return random.nextDouble() * max;
    }

    public static double getDouble(double min, double max) {
        return CommonMath.lerp(random.nextDouble(), min, max);
    }

    public static Boolean oneIn(Integer chance) {return getInteger(chance) == 0;}

    public static <T> T randomElement(List<T> collection) {
        return collection.get(getInteger(collection.size()));
    }
}
