package roguelike.core;

import java.util.HashMap;
import java.util.Map;

public class Noise {
    static Map<String, LayeredNoise> noiseMap = new HashMap<String, LayeredNoise>();

    public static LayeredNoise addNoise(String name) {
        return addNoise(name, 4, 0.5, 2.0);
    }

    public static LayeredNoise addNoise(String name, Integer octaves, double persistence, double roughness) {
        noiseMap.put(name, new LayeredNoise(octaves, persistence, roughness));
        return noiseMap.get(name);
    }

    public static double get2D(String name, double x, double y) {
        return noiseMap.get(name).get2D(x, y);
    }
}