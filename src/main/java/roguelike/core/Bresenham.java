package roguelike.core;

import java.util.ArrayList;
import java.util.List;

public class Bresenham {

    public static List<Vector2i> line(Vector2i start, Vector2i target) {
        List<Vector2i> points = new ArrayList<Vector2i>();
        int deltaX = Math.abs(target.getX() - start.getX());
        int deltaY = -Math.abs(target.getY() - start.getY());

        int sX = (start.getX() < target.getX()) ? 1 : -1;
        int sY = (start.getY() < target.getY()) ? 1 : -1;

        int error = deltaX + deltaY;

        Vector2i current = start;

        for (int iterations = 0; iterations < 100; ++iterations) {

            points.add(current);

            if (current.equals(target)) {
                break;
            }
            int error2 = 2 * error;
            if (error2 >= deltaY) {
                error += deltaY;
                current = current.add(new Vector2i(sX, 0));
            }
            if (error2 <= deltaX) {
                error += deltaX;
                current = current.add(new Vector2i(0, sY));
            }

        }

        return points;

    }

}
