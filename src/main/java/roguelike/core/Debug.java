package roguelike.core;

public class Debug {

    public static void print(String message) {
        System.out.println("[DEBUG] " + message);
    }

    public static void printLocalTime(String message) {
        System.out.println("[DEBUG] " + System.currentTimeMillis() + " " + message);
    }

}
