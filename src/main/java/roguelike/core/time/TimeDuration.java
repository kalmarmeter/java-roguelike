package roguelike.core.time;

public class TimeDuration {
    long value;

    public static final TimeDuration ZERO = new TimeDuration(0L);
    public static final TimeDuration MAX = new TimeDuration(Long.MAX_VALUE);

    public static final TimeDuration SECOND = new TimeDuration(1L);
    public static final TimeDuration MINUTE = new TimeDuration(SECOND.value * 60L);
    public static final TimeDuration HOUR = new TimeDuration(MINUTE.value * 60L);

    TimeDuration(long value) {
        this.value = value;
    }

    public Long getValue() {
        return value;
    }

    public static TimeDuration seconds(long value) {
        return new TimeDuration(value * SECOND.value);
    }

    public static TimeDuration minutes(long value) {
        return new TimeDuration(value * MINUTE.value);
    }

    public static TimeDuration hours(long value) {
        return new TimeDuration(value * HOUR.value);
    }

    public Long asSeconds() {
        return value / SECOND.value;
    }

    public Long asMinutes() {
        return value / MINUTE.value;
    }

    public Long asHours() {
        return value / HOUR.value;
    }

    public TimeDuration add(TimeDuration other) {
        return new TimeDuration(this.value + other.value);
    }

    public TimeDuration subtract(TimeDuration other) {
        return new TimeDuration(this.value - other.value);
    }

    public boolean shorterThan(TimeDuration other) {
        return this.value < other.value;
    }

    public boolean longerThan(TimeDuration other) {
        return this.value > other.value;
    }

    public TimeDuration multiply(float value) {
        return new TimeDuration(Math.round(this.value * value));
    }
}
