package roguelike.core.time;

public class TimePoint {
    long value;

    public static final TimePoint ZERO = new TimePoint(0L);
    public static final TimePoint MAX = new TimePoint(Long.MAX_VALUE);

    public TimePoint(long value) {
        this.value = value;
    }

    public TimePoint copy() {
        return new TimePoint(this.value);
    }

    public Long getValue() {
        return value;
    }

    public TimePoint advance(TimeDuration duration) {
        this.value += duration.getValue();
        return this;
    }

    public TimePoint add(TimeDuration duration) {
        return new TimePoint(this.value + duration.getValue());
    }

    public TimeDuration durationTo(TimePoint other) {
        return new TimeDuration(other.value - this.value);
    }

    public boolean every(TimeDuration period) {
        return (this.value % period.getValue() == 0);
    }

    public boolean before(TimePoint other) {
        return this.value < other.value;
    }

    public boolean after(TimePoint other) {
        return this.value > other.value;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }

        if (other instanceof TimePoint) {
            return (((TimePoint) other).value == this.value);
        }

        return false;
    }

    public long getSeconds() {
        return (this.value / TimeDuration.SECOND.getValue()) % 60;
    }

    public long getMinutes() {
        return (this.value / TimeDuration.MINUTE.getValue()) % 60;
    }

    public long getHours() {
        return this.value / TimeDuration.HOUR.getValue();
    }

    @Override
    public String toString() {
        return String.format("%02d", getHours()) + ":" + String.format("%02d", getMinutes()) + ":" + String.format("%02d", getSeconds());
    }
}
