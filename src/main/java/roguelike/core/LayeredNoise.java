package roguelike.core;

public class LayeredNoise {
    OpenSimplexNoise noise;
    Integer octaves;
    double persistence;
    double roughness;

    public LayeredNoise(Integer octaves, double persistence, double roughness) {
        noise = new OpenSimplexNoise(Random.getInteger());

        this.octaves = octaves;
        this.persistence = persistence;
        this.roughness = roughness;
    }

    public double get2D(double x, double y) {
        double noiseValue = 0.0;
        double frequency = 1.0;
        double amplitude = 1.0;
        double maxValue = 0.0;
        for (int i = 0; i < octaves; ++i) {
            maxValue += amplitude;
            noiseValue += (noise.eval(x * frequency, y * frequency) / 2.0 + 0.5) * amplitude;
            frequency *= roughness;
            amplitude *= persistence;

        }

        return noiseValue / maxValue;
    }

}