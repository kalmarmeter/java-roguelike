package roguelike.core;

public class CommonMath {

    public static final double PI = Math.PI;
    public static final double TWO_PI = PI * 2.0;
    public static final double HALF_PI = PI / 2.0;
    public static final double RADIAN = 57.296;

    public static Integer clamp(Integer value, Integer min, Integer max) {
        return Math.max(min, Math.min(max, value));
    }

    public static double lerp(double t, double a, double b) {
        return a + t * (b - a);
    }

    public static Boolean isInsideRange(Integer value, Integer min, Integer max) {
        return (value >= min && value <= max);
    }

    public static double fromDegrees(double degrees) {
        return degrees / RADIAN;
    }

    public static double fromRadians(double radians) {
        return radians / PI * 180.0;
    }

    public static double max(double... values) {
        double max = values[0];
        for (int i = 1; i < values.length; ++i) {
            if (values[i] > max) {
                max = values[i];
            }
        }

        return max;
    }

    public static double min(double... values) {
        double min = values[0];
        for (int i = 1; i < values.length; ++i) {
            if (values[i] < min) {
                min = values[i];
            }
        }

        return min;
    }
}
