package roguelike.core;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;

import java.util.ArrayList;
import java.util.List;

public class Vector2i {

    Integer x;
    Integer y;

    public static final Vector2i ZERO = new Vector2i();
    public static final Vector2i UP = new Vector2i(0, -1);
    public static final Vector2i DOWN = new Vector2i(0, 1);
    public static final Vector2i LEFT = new Vector2i(-1, 0);
    public static final Vector2i RIGHT = new Vector2i(1, 0);

    public Vector2i(Integer x, Integer y) {
        this.x = x;
        this.y = y;
    }

    public Vector2i() {
        this.x = 0;
        this.y = 0;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public Vector2i justX() {
        return new Vector2i(this.x, 0);
    }

    public Vector2i justY() {
        return new Vector2i(0, this.y);
    }

    public Vector2i add(Vector2i other) {
        return new Vector2i(this.x + other.x, this.y + other.y);
    }

    public Vector2i subtract(Vector2i other) {
        return new Vector2i(this.x - other.x, this.y - other.y);
    }

    public Integer dot(Vector2i other) {
        return (this.x * other.x + this.y * other.y);
    }

    public Integer lengthSquared() {
        return (this.x * this.x + this.y * this.y);
    }

    public Double length() {
        return Math.sqrt(lengthSquared().doubleValue());
    }

    public Vector2i negate() {
        return new Vector2i(-this.x, -this.y);
    }

    public Double distanceTo(Vector2i other) {
        return this.subtract(other).length();
    }

    public Integer manhattanLength() {
        return Math.abs(x) + Math.abs(y);
    }

    public Vector2i multiplyFloor(float value) {
        return new Vector2i((int)(this.x * value), (int)(this.y * value));
    }

    public Vector2i multiply(float value) {
        return new Vector2i((int)Math.round(this.x * value), (int)Math.round(this.y * value));
    }

    public double getAngle() {
        return Math.atan2(y, x);
    }

    public TerminalPosition toTerminalPosition() {
        if (this.x < 0 || this.y < 0) {
            return TerminalPosition.TOP_LEFT_CORNER;
        } else {
            return new TerminalPosition(this.x, this.y);
        }
    }

    public TerminalSize toTerminalSize() {
        if (this.x < 0 || this.y < 0) {
            return TerminalSize.ZERO;
        } else {
            return new TerminalSize(x, y);
        }
    }

    public List<Vector2i> getNeighbours() {
        List<Vector2i> result = new ArrayList<Vector2i>();
        result.add(this.add(Vector2i.UP));
        result.add(this.add(Vector2i.LEFT));
        result.add(this.add(Vector2i.DOWN));
        result.add(this.add(Vector2i.RIGHT));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }

        if (other instanceof Vector2i) {
            return x.equals(((Vector2i)other).x) && y.equals(((Vector2i)other).y);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return (x + y) * (x * x * x - y * y) + x * y;
    }

    public long hashLong() {
        final long a = 2862933555777941757L;
        return (a + y) * a + x;
    }

    @Override
    public String toString() {
        return "( " + x + " , " + y + " )";
    }

    public static Vector2i fromLengthAndAngle(double length, double angle) {
        return new Vector2i((int)(Math.round(Math.cos(angle) * length)), (int)(Math.round(Math.sin(angle) * length)));
    }
}
