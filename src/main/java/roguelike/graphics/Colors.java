package roguelike.graphics;

import com.googlecode.lanterna.TextCharacter;
import com.googlecode.lanterna.TextColor;

public class Colors {

    public static final TextColor BLACK = new TextColor.RGB(0, 0, 0);
    public static final TextColor WHITE = new TextColor.RGB(255, 255, 255);
    public static final TextColor GRAY = new TextColor.RGB(127, 127, 127);
    public static final TextColor RED = new TextColor.RGB(255, 0, 0);
    public static final TextColor GREEN = new TextColor.RGB(0, 255, 0);
    public static final TextColor BLUE = new TextColor.RGB(0, 0, 255);
    public static final TextColor YELLOW = new TextColor.RGB(255, 255, 0);
    public static final TextColor PINK = new TextColor.RGB(255, 0, 255);
    public static final TextColor TURQUOISE = new TextColor.RGB(0, 255, 255);
    public static final TextColor ORANGE = new TextColor.RGB(255, 165, 0);
    public static final TextColor BROWN = new TextColor.RGB(139, 69, 19);
    public static final TextColor DARK_GRAY = new TextColor.RGB(64, 64, 64);
    public static final TextColor LIGHT_BROWN = new TextColor.RGB(210, 105, 30);
    public static final TextColor DARK_GREEN = new TextColor.RGB(0, 127, 0);
    public static final TextColor MID_GREEN = new TextColor.RGB(0, 190, 0);
    public static final TextColor MID_RED = new TextColor.RGB(190, 0, 0);

    public static String toRGBTag(TextColor color) {
        TextColor.RGB rgbColor = (TextColor.RGB)color;
        return new StringBuilder("<").
                append(rgbColor.getRed()).
                append(",").
                append(rgbColor.getGreen()).
                append(",").
                append(rgbColor.getBlue()).
                append(">").
                toString();
    }

    public static TextColor fromRGBTag(String tag) {
        String[] values = tag.substring(1, tag.length() - 1).split(",");
        return new TextColor.RGB(
                Integer.parseInt(values[0]),
                Integer.parseInt(values[1]),
                Integer.parseInt(values[2]));
    }

    public static TextColor.RGB tint(TextColor color, double r, double g, double b) {
        TextColor.RGB rgbColor = (TextColor.RGB)color;
        return new TextColor.RGB(
                (int)(rgbColor.getRed() * r),
                (int)(rgbColor.getGreen() * g),
                (int)(rgbColor.getBlue() * b)
        );
    }

    public static TextColor.RGB shade(TextColor color, double factor) {
        return tint(color, factor, factor, factor);
    }

}
