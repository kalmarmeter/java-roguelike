package roguelike.graphics.ui;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextCharacter;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;
import roguelike.core.Pair;
import roguelike.main.Game;
import roguelike.main.GameState;
import roguelike.core.Rectanglei;
import roguelike.core.Vector2i;
import roguelike.entity.Entity;
import roguelike.graphics.Colors;
import roguelike.graphics.Glyphs;
import roguelike.map.Tile;
import roguelike.map.TileProperties;
import roguelike.text.Text;
import roguelike.weapon.Gun;

import java.util.ArrayList;
import java.util.List;

public class UIRenderer {
    Game game;
    TextGraphics textGraphics;
    Rectanglei gameScreen;

    public UIRenderer(Game game, TextGraphics textGraphics, Vector2i gameScreenOffset) {
        this.game = game;
        this.textGraphics = textGraphics;
        this.gameScreen = new Rectanglei(gameScreenOffset, game.getCamera().getRectangle().getSize());
    }

    public void draw() {
        drawBorderAround(gameScreen);
        drawBorderAround(new Rectanglei(gameScreen.topRight().add(new Vector2i(3, 0)), new Vector2i(26, gameScreen.getHeight())));
        drawBorderAround(new Rectanglei(gameScreen.bottomLeft().add(new Vector2i(0, 3)), new Vector2i(gameScreen.getWidth() + 28, 3)));
        drawGameLog();
        textGraphics.fillRectangle(gameScreen.topRight().add(new Vector2i(3, 0)).toTerminalPosition(),
                new TerminalSize(26, gameScreen.getHeight()), Glyphs.EMPTY);
        if (game.getGameState() == GameState.CONTROLLING_PLAYER) {
            drawActionInfo();
            drawPlayerInfo();
        } else if (game.getGameState() == GameState.SELECTING_TILE) {
            drawSelectedTileInfo();
        }
    }

    void drawBorderAround(Rectanglei area) {
        textGraphics.drawLine(area.topLeft().add(Vector2i.UP).toTerminalPosition(),
                area.topRight().add(Vector2i.UP).toTerminalPosition(),
                Glyphs.BORDER_HORIZONTAL);

        textGraphics.drawLine(area.topLeft().add(Vector2i.LEFT).toTerminalPosition(),
                area.bottomLeft().add(Vector2i.LEFT).toTerminalPosition(),
                Glyphs.BORDER_VERTICAL);

        textGraphics.drawLine(area.bottomLeft().add(Vector2i.DOWN).toTerminalPosition(),
                area.bottomRight().add(Vector2i.DOWN).toTerminalPosition(),
                Glyphs.BORDER_HORIZONTAL);

        textGraphics.drawLine(area.topRight().add(Vector2i.RIGHT).toTerminalPosition(),
                area.bottomRight().add(Vector2i.RIGHT).toTerminalPosition(),
                Glyphs.BORDER_VERTICAL);

        textGraphics.setCharacter(area.topLeft().add(new Vector2i(-1, -1)).toTerminalPosition(),
                Glyphs.BORDER_TOP_LEFT);

        textGraphics.setCharacter(area.bottomLeft().add(new Vector2i(-1, 1)).toTerminalPosition(),
                Glyphs.BORDER_BOTTOM_LEFT);

        textGraphics.setCharacter(area.bottomRight().add(new Vector2i(1, 1)).toTerminalPosition(),
                Glyphs.BORDER_BOTTOM_RIGHT);

        textGraphics.setCharacter(area.topRight().add(new Vector2i(1, -1)).toTerminalPosition(),
                Glyphs.BORDER_TOP_RIGHT);
    }

    void drawGameLog() {
        Rectanglei textArea = new Rectanglei(gameScreen.bottomLeft().add(new Vector2i(0, 3)), new Vector2i(gameScreen.getWidth() + 28, 3));
        textGraphics.fillRectangle(textArea.topLeft().toTerminalPosition(), textArea.getSize().toTerminalSize(), ' ');
        int lines = Math.min(game.getMessageLog().size(), 3);
        for (int i = 0; i < lines; ++i) {
            drawString(gameScreen.bottomLeft().add(new Vector2i(0, 3 + lines - i - 1)), game.getMessageLog().get(game.getMessageLog().size() - i - 1), Colors.WHITE,
                    gameScreen.getWidth() + 28);
        }
    }

    void drawActionInfo() {
        List<String> lines = new ArrayList<String>();
        lines.add("Available actions:");
        lines.add("[w] North: " +
                (game.getMap().getMapArea().contains(game.getPlayer().getPosition().add(Vector2i.UP)) ?
                game.getMovementCost(game.getPlayer(), game.getPlayer().getPosition().add(Vector2i.UP)).asSeconds() :
                "-")
                + " s");
        lines.add("[a] West: " +
                (game.getMap().getMapArea().contains(game.getPlayer().getPosition().add(Vector2i.LEFT)) ?
                        game.getMovementCost(game.getPlayer(), game.getPlayer().getPosition().add(Vector2i.LEFT)).asSeconds() :
                        "-")
                + " s");
        lines.add("[s] South: " +
                (game.getMap().getMapArea().contains(game.getPlayer().getPosition().add(Vector2i.DOWN)) ?
                        game.getMovementCost(game.getPlayer(), game.getPlayer().getPosition().add(Vector2i.DOWN)).asSeconds() :
                        "-")
                + " s");
        lines.add("[d] East: " +
                (game.getMap().getMapArea().contains(game.getPlayer().getPosition().add(Vector2i.RIGHT)) ?
                        game.getMovementCost(game.getPlayer(), game.getPlayer().getPosition().add(Vector2i.RIGHT)).asSeconds() :
                        "-")
                + " s");
        lines.add("[k] Select tile / Aim");
        lines.add("[g] Shoot last target");
        lines.add("[r] Reload " + ((Gun)game.getPlayer().getCurrentWeapon()).getBaseReloadTime().asSeconds() + " s");
        drawMultilineStrings(gameScreen.topRight().add(new Vector2i(3, 0)), Colors.WHITE,
                lines);
    }

    void drawSelectedTileInfo() {
        Tile selectedTile = game.getMap().getTile(game.getSelectedTile());
        drawMultilineStrings(gameScreen.topRight().add(new Vector2i(3, 0)), Colors.WHITE,
                "Selected Tile:",
                "Name: " + selectedTile.getType().toString(),
                "Height: " + TileProperties.getTileProperty(selectedTile).getHeight().toString(),
                (selectedTile.isOnFire() ? "[ON FIRE]" : ""));
        Entity entity = game.getMap().entityAt(game.getSelectedTile());
        if (entity != null) {
            drawMultilineStrings(gameScreen.topRight().add(new Vector2i(3, 5)), Colors.WHITE,
                    "Entity at selection:",
                    "Name: " + entity.toString(),
                    "Health: " + entity.getHP(),
                    "Action: " + entity.getCurrentAction());
        }
    }

    void drawPlayerInfo() {
        drawMultilineStrings(gameScreen.topRight().add(new Vector2i(3, 8)),
                Colors.YELLOW,
                "Health: " + game.getPlayer().getHP() + " / 100",
                "Stance: " + game.getPlayer().getStance().toString(),
                "Weapon: " + game.getPlayer().getCurrentWeapon(),
                "Ammo: " + ((Gun)game.getPlayer().getCurrentWeapon()).getAmmo() + "/" + ((Gun)game.getPlayer().getCurrentWeapon()).getMaxAmmo());
    }

    void drawString(Vector2i position, String string, TextColor color) {
        drawString(position.toTerminalPosition(), string, color);
    }

    void drawString(Vector2i position, String string, TextColor color, int maxLength) {
        drawString(position.toTerminalPosition(), string, color, maxLength);
    }

    void drawString(TerminalPosition position, String string, TextColor color) {
        for (int i = 0; i < string.length(); ++i) {
            char ch = string.charAt(i);
            textGraphics.setCharacter(position.withRelativeColumn(i), new TextCharacter(ch, color, Colors.BLACK));
        }
    }

    void drawString(TerminalPosition position, String string, TextColor color, int maxLength) {
        int length = Math.min(string.length(), maxLength);
        for (int i = 0; i < length; ++i) {
            char ch = string.charAt(i);
            textGraphics.setCharacter(position.withRelativeColumn(i), new TextCharacter(ch, color, Colors.BLACK));
        }
    }

    void drawColoredString(Vector2i startPosition, String string) {
        List<Pair<String, TextColor>> list = Text.parseColoredString(string);
        for (Pair<String, TextColor> pair : list) {
            drawString(startPosition, pair.getFirst(), pair.getSecond());
            startPosition = startPosition.add(new Vector2i(pair.getFirst().length(), 0));
        }
    }

    void drawMultilineStrings(Vector2i startPosition, TextColor color, String... lines) {
        for (String line : lines) {
            drawString(startPosition, line, color);
            startPosition = startPosition.add(Vector2i.DOWN);
        }
    }

    void drawMultilineStrings(Vector2i startPosition, TextColor color, int maxLength, String... lines) {
        for (String line : lines) {
            drawString(startPosition, line, color, maxLength);
            startPosition = startPosition.add(Vector2i.DOWN);
        }
    }

    void drawMultilineStrings(Vector2i startPosition, TextColor color, List<String> lines) {
        for (String line : lines) {
            drawString(startPosition, line, color);
            startPosition = startPosition.add(Vector2i.DOWN);
        }
    }

    void drawMultilineStrings(Vector2i startPosition, TextColor color, int maxLength, List<String> lines) {
        for (String line : lines) {
            drawString(startPosition, line, color, maxLength);
            startPosition = startPosition.add(Vector2i.DOWN);
        }
    }
}
