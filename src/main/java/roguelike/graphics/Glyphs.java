package roguelike.graphics;

import com.googlecode.lanterna.SGR;
import com.googlecode.lanterna.Symbols;
import com.googlecode.lanterna.TextCharacter;

public class Glyphs {

    public static TextCharacter getGrass(Integer hash) {
        int index = hash % 4 + 1;
        if (index == 1) {
            return GRASS_1;
        } else if (index == 2) {
            return GRASS_2;
        } else if (index == 3) {
            return GRASS_3;
        } else {
            return GRASS_4;
        }
    }

    public static TextCharacter getTree(Integer hash) {
        int index = hash % 4 + 1;
        if (index == 1) {
            return TREE_1;
        } else if (index == 2) {
            return TREE_2;
        } else {
            return TREE_3;
        }
    }

    public static final TextCharacter EMPTY =
            new TextCharacter(' ', Colors.GRAY, Colors.BLACK);

    //PLAYER GLYPHS
    public static final TextCharacter PLAYER_DEFAULT =
            new TextCharacter('@', Colors.WHITE, Colors.BLACK);

    //ENTITY GLYPHS
    public static final TextCharacter ENTITY_TEST =
            new TextCharacter('E', Colors.RED, Colors.BLACK);
    public static final TextCharacter ROCKET_TEST =
            new TextCharacter('*', Colors.YELLOW, Colors.BLACK);

    //TILE GLYPHS
    public static final TextCharacter WALL =
            new TextCharacter(Symbols.BLOCK_DENSE, Colors.GRAY, Colors.BLACK);
    public static final TextCharacter DIRT =
            new TextCharacter('.', Colors.BLACK, Colors.BROWN);
    public static final TextCharacter GRASS_1 =
            new TextCharacter(' ', Colors.DARK_GREEN, Colors.DARK_GREEN);
    public static final TextCharacter GRASS_2 =
            new TextCharacter('.', Colors.GREEN, Colors.DARK_GREEN);
    public static final TextCharacter GRASS_3 =
            new TextCharacter(',', Colors.MID_GREEN, Colors.DARK_GREEN);
    public static final TextCharacter GRASS_4 =
            new TextCharacter(' ', Colors.DARK_GREEN, Colors.DARK_GREEN);
    public static final TextCharacter SNOW_1 =
            new TextCharacter('.', Colors.WHITE, Colors.BLACK);
    public static final TextCharacter MUD =
            new TextCharacter('~', Colors.BROWN, Colors.BLACK);
    public static final TextCharacter WATER =
            new TextCharacter('≈', Colors.TURQUOISE, Colors.BLUE);
    public static final TextCharacter TREE_1 =
            new TextCharacter('♣', Colors.GREEN, Colors.DARK_GREEN);
    public static final TextCharacter TREE_2 =
            new TextCharacter('☘', Colors.GREEN, Colors.DARK_GREEN);
    public static final TextCharacter TREE_3 =
            new TextCharacter('♠', Colors.GREEN, Colors.DARK_GREEN);
    public static final TextCharacter PINE_1 =
            new TextCharacter('♠', Colors.WHITE, Colors.BLACK);
    public static final TextCharacter FIRE =
            new TextCharacter(Symbols.TRIANGLE_UP_POINTING_BLACK, Colors.ORANGE, Colors.BLACK);
    public static final TextCharacter BRANCHES =
            new TextCharacter('⊪', Colors.LIGHT_BROWN, Colors.BLACK);
    public static final TextCharacter RUBBLE =
            new TextCharacter('⊪', Colors.DARK_GRAY, Colors.BLACK);

    //ANIMATION GLYPHS
    public static final TextCharacter TRANSPARENT =
            new TextCharacter(' ', Colors.PINK, Colors.BLACK);
    public static final TextCharacter EXPLOSION_YELLOW =
            new TextCharacter(Symbols.BLOCK_DENSE, Colors.YELLOW, Colors.BLACK);
    public static final TextCharacter EXPLOSION_ORANGE =
            new TextCharacter(Symbols.BLOCK_DENSE, Colors.ORANGE, Colors.BLACK);
    public static final TextCharacter SMOKE_1 =
            new TextCharacter('≋', Colors.GRAY, Colors.BLACK);
    public static final TextCharacter SMOKE_2 =
            new TextCharacter('≊', Colors.DARK_GRAY, Colors.BLACK);

    //UI GLYPHS
    public static final TextCharacter BORDER_HORIZONTAL =
            new TextCharacter(Symbols.DOUBLE_LINE_HORIZONTAL, Colors.ORANGE, Colors.BLACK);
    public static final TextCharacter BORDER_VERTICAL =
            new TextCharacter(Symbols.DOUBLE_LINE_VERTICAL, Colors.ORANGE, Colors.BLACK);
    public static final TextCharacter BORDER_TOP_LEFT =
            new TextCharacter(Symbols.DOUBLE_LINE_TOP_LEFT_CORNER, Colors.ORANGE, Colors.BLACK);
    public static final TextCharacter BORDER_TOP_RIGHT =
            new TextCharacter(Symbols.DOUBLE_LINE_TOP_RIGHT_CORNER, Colors.ORANGE, Colors.BLACK);
    public static final TextCharacter BORDER_BOTTOM_LEFT =
            new TextCharacter(Symbols.DOUBLE_LINE_BOTTOM_LEFT_CORNER, Colors.ORANGE, Colors.BLACK);
    public static final TextCharacter BORDER_BOTTOM_RIGHT =
            new TextCharacter(Symbols.DOUBLE_LINE_BOTTOM_RIGHT_CORNER, Colors.ORANGE, Colors.BLACK);
    public static final TextCharacter CROSSHAIR_1 =
            new TextCharacter('⊹', Colors.RED, Colors.BLACK, SGR.BOLD);
    public static final TextCharacter CROSSHAIR_2 =
            new TextCharacter('⊹', Colors.BLACK, Colors.RED, SGR.BOLD);
}
