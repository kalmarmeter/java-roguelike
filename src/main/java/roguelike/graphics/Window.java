package roguelike.graphics;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.screen.TerminalScreen;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.googlecode.lanterna.terminal.Terminal;

import com.googlecode.lanterna.terminal.swing.AWTTerminalFontConfiguration;
import roguelike.main.Game;
import roguelike.graphics.game.Camera;
import roguelike.core.Vector2i;
import roguelike.graphics.game.GameRenderer;
import roguelike.graphics.ui.UIRenderer;

import java.awt.*;
import java.io.IOException;

public class Window {

    Terminal terminal;
    Screen screen;
    TextGraphics textGraphics;

    Game game;

    GameRenderer gameRenderer;
    UIRenderer uiRenderer;

    public Window() {

    }

    public Terminal getTerminal() {
        return terminal;
    }

    public void initialize(Game game) throws IOException {
        this.game = game;

        DefaultTerminalFactory defaultTerminalFactory = new DefaultTerminalFactory();
        //Does not work yet
        defaultTerminalFactory.setTerminalEmulatorFontConfiguration(
                AWTTerminalFontConfiguration.newInstance(new Font("Consolas", Font.BOLD, 24))
        );
        defaultTerminalFactory.setInitialTerminalSize(
                new TerminalSize(Camera.DEFAULT_SIZE.getX() + 30, Camera.DEFAULT_SIZE.getY() + 7));

        terminal = defaultTerminalFactory.createTerminal();
        terminal.setCursorVisible(false);
        screen = new TerminalScreen(terminal);
        textGraphics = screen.newTextGraphics();

        Vector2i gameScreenOffset = new Vector2i(1, 1);

        gameRenderer = new GameRenderer(game, textGraphics, gameScreenOffset);
        uiRenderer = new UIRenderer(game, textGraphics, gameScreenOffset);

        screen.startScreen();
        
    }

    public void cleanup() throws IOException {
        screen.stopScreen();
        screen.close();
        terminal.close();
    }

    public void clear() {
        //screen.clear();
    }

    public void draw() {
        uiRenderer.draw();
        gameRenderer.draw();
    }

    public void display() throws IOException {
        screen.refresh();
    }

    public GameRenderer getGameRenderer() {
        return gameRenderer;
    }

    public UIRenderer getUiRenderer() {
        return uiRenderer;
    }
}
