package roguelike.graphics.game;

import com.googlecode.lanterna.TextCharacter;
import com.googlecode.lanterna.graphics.TextGraphics;
import roguelike.core.LayeredNoise;
import roguelike.core.Noise;
import roguelike.main.Game;
import roguelike.main.GameState;
import roguelike.core.Pair;
import roguelike.core.Vector2i;
import roguelike.entity.Entity;
import roguelike.entity.entities.TestEnemy;
import roguelike.entity.entities.TestRocket;
import roguelike.graphics.Colors;
import roguelike.graphics.Glyphs;
import roguelike.map.Map;
import roguelike.map.Tile;
import roguelike.player.Player;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GameRenderer {

    Game game;
    TextGraphics textGraphics;
    Camera camera;
    Vector2i screenOffset;
    List<Pair<Animation, AnimationAttributes>> currentAnimations;
    Long framesElapsed;
    LayeredNoise cloudNoise = Noise.addNoise("clouds", 1, 1.0, 1.0);

    public GameRenderer(Game game, TextGraphics textGraphics, Vector2i screenOffset) {
        this.game = game;
        this.camera = game.getCamera();
        this.textGraphics = textGraphics;
        this.screenOffset = screenOffset;
        this.currentAnimations = new ArrayList<Pair<Animation, AnimationAttributes>>();
        this.framesElapsed = 0L;
    }

    public void draw() {
        drawMap(game.getMap());
        drawPlayer(game.getPlayer());
        drawEntities(game.getEntities());
        updateAnimations();
        drawAnimations();
        if (game.getGameState() == GameState.SELECTING_TILE) {
            drawSelection(game.getSelectedTile());
        }

        ++framesElapsed;
    }

    void drawMap(Map map) {
        for (Vector2i position : camera.getRectangle()) {
            Tile tile = map.getTile(position);
            TextCharacter ch;
            if (game.getPlayer().isPositionVisible(position)) {

                if (tile.isOnFire()) {
                    ch = Glyphs.FIRE;
                } else {
                    switch (tile.getType()) {
                        case WALL:
                            ch = Glyphs.WALL;
                            break;
                        case DIRT:
                            ch = Glyphs.DIRT;
                            break;
                        case GRASS:
                            ch = Glyphs.getGrass(tile.getRandomID());
                            break;
                        case SNOW:
                            ch = Glyphs.SNOW_1;
                            break;
                        case MUD:
                            ch = Glyphs.MUD;
                            break;
                        case WATER:
                            ch = Glyphs.WATER;
                            break;
                        case OAK_TREE:
                            ch = Glyphs.getTree(tile.getRandomID());
                            break;
                        case PINE_TREE:
                            ch = Glyphs.PINE_1;
                            break;
                        case RUBBLE:
                            ch = Glyphs.RUBBLE;
                            break;
                        case BRANCHES:
                            ch = Glyphs.BRANCHES;
                            break;
                        default:
                            ch = Glyphs.EMPTY;
                            break;
                    }

                    if (tile.isBloody()) {
                        ch = new TextCharacter(ch.getCharacter(), Colors.MID_RED, ch.getBackgroundColor());
                    }
                }

                double cloudCover = Noise.get2D("clouds",
                        (position.getX() + framesElapsed / 10) / 25.0,
                        (position.getY() + framesElapsed / 20) / 25.0);
                double lightLevel = 1.0;
                ch = new TextCharacter(ch.getCharacter(), Colors.shade(ch.getForegroundColor(), lightLevel), ch.getBackgroundColor());

            } else {
                ch = Glyphs.EMPTY;
            }

            Vector2i onScreen = getScreenPosition(position);
            textGraphics.setCharacter(onScreen.toTerminalPosition(), ch);
        }
    }

    void drawPlayer(Player player) {
        textGraphics.setCharacter(
                getScreenPosition(player.getPosition()).toTerminalPosition(),
                Glyphs.PLAYER_DEFAULT
        );
    }

    void drawSelection(Vector2i selection) {
        if (framesElapsed / 45 % 2 == 0) {
            textGraphics.setCharacter(
                    getScreenPosition(selection).toTerminalPosition(),
                    Glyphs.CROSSHAIR_1
            );
        } else {
            textGraphics.setCharacter(
                    getScreenPosition(selection).toTerminalPosition(),
                    Glyphs.CROSSHAIR_2
            );
        }
    }

    void drawEntities(List<Entity> entities) {
        for (Entity entity : entities) {
            if (!entity.isDead() && camera.getRectangle().contains(entity.getPosition()) && game.getPlayer().isPositionVisible(entity.getPosition())) {
                TextCharacter ch = null;
                if (entity instanceof TestEnemy) {
                    ch = Glyphs.ENTITY_TEST;
                } else if (entity instanceof TestRocket) {
                    ch = Glyphs.ROCKET_TEST;
                }
                textGraphics.setCharacter(
                        getScreenPosition(entity.getPosition()).toTerminalPosition(),
                        ch
                );
            }
        }
    }

    void updateAnimations() {
        Iterator<Pair<Animation, AnimationAttributes>> iterator = currentAnimations.iterator();
        while (iterator.hasNext()) {
            Pair<Animation, AnimationAttributes> pair = iterator.next();
            Animation animation = pair.getFirst();
            AnimationAttributes attributes = pair.getSecond();
            animation.advance(attributes.getSpeed());
            if (animation.isFinished()) {
                iterator.remove();
            }
        }
    }

    public void addAnimation(Animation animation, AnimationAttributes attributes) {
        currentAnimations.add(new Pair<Animation, AnimationAttributes>(animation, attributes));
    }

    void drawAnimation(Pair<Animation, AnimationAttributes> pair) {
        for (Vector2i pos : Animation.RECTANGLE) {
            TextCharacter ch = pair.getFirst().getCurrentFrame().get(pos);
            if (ch != Glyphs.TRANSPARENT) {
                textGraphics.setCharacter(getScreenPosition(pos.add(pair.getSecond().getPosition())).toTerminalPosition(), ch);
            }
        }
    }

    void drawAnimations() {
        for (Pair<Animation, AnimationAttributes> pair : currentAnimations) {
            drawAnimation(pair);
        }
    }

    Vector2i getScreenPosition(Vector2i point) {
        return camera.getRelativePosition(point).add(screenOffset);
    }

}
