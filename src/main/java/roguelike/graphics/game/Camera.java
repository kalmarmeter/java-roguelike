package roguelike.graphics.game;

import roguelike.core.CommonMath;
import roguelike.core.Rectanglei;
import roguelike.core.Vector2i;
import roguelike.entity.Entity;

public class Camera {

    public static final Vector2i DEFAULT_SIZE = new Vector2i(64, 32);

    Rectanglei rectangle;
    Rectanglei bounds = null;
    Entity entityFollowing = null;

    public Camera() {
        rectangle = new Rectanglei(new Vector2i(0,0), DEFAULT_SIZE);
    }

    public Camera(Vector2i position, Vector2i size) {
        rectangle = new Rectanglei(position, size);
    }

    public Camera(Rectanglei rectangle) {
        this.rectangle = rectangle;
    }

    public Rectanglei getRectangle() {
        if (entityFollowing != null) {
            Vector2i entityPos = entityFollowing.getPosition();
            rectangle.setPosition(
                    entityPos.subtract(rectangle.getSize().multiplyFloor(0.5f))
            );
        }

        if (bounds != null) {
            int x = CommonMath.clamp(rectangle.getPosition().getX(), bounds.left(), bounds.right() - rectangle.getWidth() + 1);
            int y = CommonMath.clamp(rectangle.getPosition().getY(), bounds.top(), bounds.bottom() - rectangle.getHeight() + 1);
            rectangle.setPosition(new Vector2i(x, y));
        }

        return rectangle;
    }

    public void setRectangle(Rectanglei rectangle) {
        this.rectangle = rectangle;
    }

    public void setBounds(Rectanglei bounds) {
        this.bounds = bounds;
    }

    public void setEntityFollowing(Entity entity) {
        this.entityFollowing = entity;
    }

    public Vector2i getRelativePosition(Vector2i position) {
        return getRectangle().getRelativePosition(position);
    }

    public Vector2i getGlobalPosition(Vector2i relativePosition) {
        return getRectangle().getGlobalPosition(relativePosition);
    }

}