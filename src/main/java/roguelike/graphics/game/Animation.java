package roguelike.graphics.game;

import com.googlecode.lanterna.TextCharacter;
import roguelike.core.Rectanglei;
import roguelike.core.Vector2i;

import java.util.ArrayList;
import java.util.List;

public class Animation {

    public static final Integer WIDTH = 8;
    public static final Integer HEIGHT = 8;

    public static final Rectanglei RECTANGLE = new Rectanglei(Vector2i.ZERO, new Vector2i(WIDTH, HEIGHT));

    List<AnimationFrame> frames;
    Integer currentTime;
    Integer currentFrame;
    Boolean finished;
    Boolean looping;

    protected static class AnimationFrame {
        TextCharacter[][] characters;
        Integer frameTime;

        public AnimationFrame(TextCharacter[][] characters, Integer frameTime) {
            this.characters = characters;
            this.frameTime = frameTime;
        }

        public Integer getFrameTime() {
            return frameTime;
        }

        public TextCharacter get(Vector2i position) {
            return characters[position.getX()][position.getY()];
        }

        public void set(Vector2i position, TextCharacter character) {
            characters[position.getX()][position.getY()] = character;
        }
    }

    protected Animation(List<AnimationFrame> frames, Boolean looping) {
        this.frames = frames;
        this.looping = looping;
        this.finished = false;
        this.currentTime = 0;
        this.currentFrame = 0;
    }

    protected Animation(Boolean looping) {
        this(new ArrayList<AnimationFrame>(), looping);
    }

    protected List<AnimationFrame> getFrames() {
        return frames;
    }

    protected void setFrames(List<AnimationFrame> frames) {
        this.frames = frames;
    }

    protected void addFrame(AnimationFrame frame) {
        frames.add(frame);
    }

    public void advance(Integer framesAdvanced) {
        if (!finished) {
            this.currentTime += framesAdvanced;
            if (frames.get(currentFrame).getFrameTime() <= currentTime) {
                currentTime -= frames.get(0).getFrameTime();
                if (currentFrame < frames.size() - 1) {
                    currentFrame++;
                } else {
                    if (looping) {
                        currentFrame = 0;
                    } else {
                        finished = true;
                    }
                }
            }
        }
    }

    public AnimationFrame getCurrentFrame() {
        return frames.get(currentFrame);
    }

    public Boolean isFinished() {
        return finished;
    }

    static public Vector2i asCenter(Vector2i pos) {
        return pos.subtract(RECTANGLE.getSize().multiplyFloor(0.5f));
    }
}
