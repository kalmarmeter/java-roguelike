package roguelike.graphics.game;

import roguelike.core.Vector2i;
import roguelike.entity.Entity;

public class AnimationAttributes {
    Vector2i position = null;
    Entity entity = null;
    Integer speed;
    Boolean centered;

    public AnimationAttributes(Vector2i position, Integer speed) {
        this(position, speed, true);
    }

    public AnimationAttributes(Entity entity, Integer speed) {
        this(entity, speed, true);
    }

    public AnimationAttributes(Vector2i position, Integer speed, boolean centered) {
        this.position = position;
        this.speed = speed;
        this.centered = centered;
    }

    public AnimationAttributes(Entity entity, Integer speed, boolean centered) {
        this.entity = entity;
        this.speed = speed;
        this.centered = centered;
    }

    public Vector2i getPosition() {
        if (entity != null) {
            if (centered) {
                return Animation.asCenter(entity.getPosition());
            } else {
                return entity.getPosition();
            }
        } else {
            if (centered) {
                return Animation.asCenter(position);
            } else {
                return position;
            }
        }
    }

    public Integer getSpeed() {
        return speed;
    }

}
