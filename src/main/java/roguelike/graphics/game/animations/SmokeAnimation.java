package roguelike.graphics.game.animations;

import com.googlecode.lanterna.TextCharacter;
import roguelike.core.Random;
import roguelike.core.Vector2i;
import roguelike.graphics.Glyphs;
import roguelike.graphics.game.Animation;

import java.util.ArrayList;
import java.util.List;

public class SmokeAnimation extends Animation {

    static List<AnimationFrame> smokeFrames = new ArrayList<AnimationFrame>();

    static {

        for (int i = 0; i < 5; ++i) {

            Vector2i center = Animation.RECTANGLE.getSize().multiplyFloor(0.5f);
            TextCharacter[][] characters = new TextCharacter[Animation.WIDTH][Animation.HEIGHT];
            for (Vector2i pos : Animation.RECTANGLE) {
                TextCharacter ch;
                if (pos.distanceTo(center) + Random.getInteger(0, 3) < 2.0f) {
                    if (Random.oneIn(2)) {
                        ch = Glyphs.SMOKE_1;
                    } else {
                        ch = Glyphs.SMOKE_2;
                    }
                } else {
                    ch = Glyphs.TRANSPARENT;
                }

                characters[pos.getX()][pos.getY()] = ch;

            }

            AnimationFrame frame = new AnimationFrame(characters, 20);
            smokeFrames.add(frame);

        }

    }

    public SmokeAnimation() {
        super(smokeFrames, false);
    }

}
