package roguelike.graphics.game.animations;

import com.googlecode.lanterna.TextCharacter;
import roguelike.core.Bresenham;
import roguelike.core.CommonMath;
import roguelike.core.Pair;
import roguelike.core.Vector2i;
import roguelike.graphics.Glyphs;
import roguelike.graphics.game.Animation;

import java.util.ArrayList;
import java.util.List;

public class ShootAnimation extends Animation {

    private ShootAnimation() {
        super(false);
        for (int frameIndex = 0; frameIndex < 2; ++frameIndex) {
            TextCharacter[][] characters = new TextCharacter[Animation.WIDTH][Animation.HEIGHT];
            for (Vector2i pos : Animation.RECTANGLE) {
                characters[pos.getX()][pos.getY()] = Glyphs.TRANSPARENT;
            }
            AnimationFrame frame = new AnimationFrame(characters, 10);
            addFrame(frame);
        }
    }

    public static List<Pair<ShootAnimation, Vector2i>> getAnimation(Vector2i start, Vector2i target) {
        List<Pair<ShootAnimation, Vector2i>> result = new ArrayList<Pair<ShootAnimation, Vector2i>>();
        List<Vector2i> points = Bresenham.line(start, target);
        Vector2i currentOffset = new Vector2i(0, 0);
        if (points.size() >= 2) {
            Vector2i point0 = points.get(0);
            Vector2i point1 = points.get(1);
            double angle = Math.atan2(point1.getY() - point0.getY(), point1.getX() - point0.getX());
            if (angle >= CommonMath.HALF_PI) {
                currentOffset = new Vector2i(-Animation.WIDTH + 1, -Animation.HEIGHT + 1);
            } else if (angle >= 0.0 && angle < CommonMath.HALF_PI) {
                currentOffset = new Vector2i(0, -Animation.HEIGHT + 1);
            } else if (angle < -CommonMath.HALF_PI) {
                currentOffset = new Vector2i(-Animation.WIDTH + 1, 0);
            }
        }
        Vector2i currentStart = start.add(currentOffset);
        ShootAnimation currentAnimation = new ShootAnimation();
        int pointsInSlice = 0;
        for (Vector2i point : points) {
            Vector2i relativePosition = point.subtract(currentStart);
            if (Animation.RECTANGLE.contains(relativePosition)) {
                ++pointsInSlice;
                for (int frameIndex = 0; frameIndex < 2; ++frameIndex) {
                    if (frameIndex == 0) {
                        currentAnimation.getFrames().get(frameIndex).set(relativePosition, Glyphs.EXPLOSION_YELLOW);
                    } else {
                        currentAnimation.getFrames().get(frameIndex).set(relativePosition, Glyphs.EXPLOSION_ORANGE);
                    }
                }
            } else {
                result.add(new Pair(currentAnimation, currentStart));
                currentStart = point.add(currentOffset);
                currentAnimation = new ShootAnimation();
                relativePosition = point.subtract(currentStart);
                for (int frameIndex = 0; frameIndex < 2; ++frameIndex) {
                    if (frameIndex == 0) {
                        currentAnimation.getFrames().get(frameIndex).set(relativePosition, Glyphs.EXPLOSION_YELLOW);
                    } else {
                        currentAnimation.getFrames().get(frameIndex).set(relativePosition, Glyphs.EXPLOSION_ORANGE);
                    }
                }
                pointsInSlice = 1;
            }
        }

        if (pointsInSlice > 0) {
            result.add(new Pair<ShootAnimation, Vector2i>(currentAnimation, currentStart));
        }

        return result;
    }
}
