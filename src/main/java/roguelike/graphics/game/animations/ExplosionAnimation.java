package roguelike.graphics.game.animations;

import com.googlecode.lanterna.TextCharacter;
import roguelike.core.Random;
import roguelike.core.Vector2i;
import roguelike.graphics.Glyphs;
import roguelike.graphics.game.Animation;

import java.util.ArrayList;
import java.util.List;

public class ExplosionAnimation extends Animation {

    Integer radius;
    static List<AnimationFrame> explosionFrames = new ArrayList<AnimationFrame>();

    static {
        Vector2i center = Animation.RECTANGLE.getSize().multiplyFloor(0.5f);
        for (int i = 0; i < 10; ++i) {
            TextCharacter[][] characters = new TextCharacter[Animation.WIDTH][Animation.HEIGHT];
            for (Vector2i pos : Animation.RECTANGLE) {
                TextCharacter ch;
                if (pos.distanceTo(center) + Random.getInteger(-1, 2) < i / 2.0f) {
                    if (Random.getInteger(2) == 0) {
                        ch = Glyphs.EXPLOSION_ORANGE;
                    } else {
                        ch = Glyphs.EXPLOSION_YELLOW;
                    }
                } else {
                    ch = Glyphs.TRANSPARENT;
                }

                characters[pos.getX()][pos.getY()] = ch;
            }
            AnimationFrame frame = new AnimationFrame(characters, 3);
            explosionFrames.add(frame);
        }
    }

    public ExplosionAnimation(Integer radius) {
        super(explosionFrames, false);
    }

}
