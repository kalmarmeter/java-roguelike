package roguelike.entity.entities;

import roguelike.ai.ais.FlyTowardsAI;
import roguelike.core.Vector2i;
import roguelike.entity.Attributes;
import roguelike.entity.Entity;

public class TestRocket extends Entity {
    public TestRocket(Vector2i position, Vector2i target) {
        super(position);
        setAI(new FlyTowardsAI(this, target));
        attributes = new Attributes(0, 10);
    }
}
