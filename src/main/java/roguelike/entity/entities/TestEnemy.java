package roguelike.entity.entities;

import roguelike.ai.ais.AStarMoveTowardsAI;
import roguelike.core.Random;
import roguelike.core.Vector2i;
import roguelike.entity.Attributes;
import roguelike.entity.Entity;
import roguelike.weapon.weapons.AssaultRifle;
import roguelike.weapon.weapons.BoltActionRifle;
import roguelike.weapon.weapons.Knife;

public class TestEnemy extends Entity {
    public TestEnemy(Vector2i position, Entity enemy) {
        super(position);
        setAI(new AStarMoveTowardsAI(this, enemy));
        attributes = new Attributes(3, 5);
        if (Random.oneIn(3)) {
            currentWeapon = new BoltActionRifle();
        } else {
            currentWeapon = new Knife();
        }
    }

}
