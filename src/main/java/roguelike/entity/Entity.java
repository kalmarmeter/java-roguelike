package roguelike.entity;

import roguelike.core.Random;
import roguelike.core.time.TimePoint;
import roguelike.main.Action;
import roguelike.player.Player;
import roguelike.ray.VisionRayTracer;
import roguelike.ai.AI;
import roguelike.core.Vector2i;
import roguelike.sound.SoundManager;
import roguelike.weapon.Weapon;

import java.util.HashSet;
import java.util.Set;

public abstract class Entity {

    protected static Integer nextID = 0;

    protected Integer ID;
    protected Vector2i position;
    protected Integer hp;
    protected boolean dead = false;
    protected Action currentAction = null;
    protected AI ai = null;
    protected Attributes attributes = null;
    protected Set<Vector2i> positionsVisible;
    protected VisionRayTracer visionRayTracer = null;
    protected Stance stance;
    protected Weapon currentWeapon;
    protected Integer accuracy;

    {
        this.ID = nextID++;
    }

    public Entity() {
        this(Vector2i.ZERO);
    }

    public Entity(Vector2i position) {
        this.position = position;
        positionsVisible = new HashSet<Vector2i>();
        //Hardcoded
        hp = 100;
        stance = Stance.STANDING;
        accuracy = 100;
    }

    public void setAI(AI ai) {
        this.ai = ai;
    }

    public Vector2i getPosition() {
        return position;
    }

    public boolean hurt(Integer damage) {
        if (this instanceof Player) {
            SoundManager.play("hurt", 30);
        } else {
            SoundManager.play("guts", 30);
        }
        this.hp -= damage;
        if (hp <= 0) {
            dead = true;
        }
        return dead;
    }

    public boolean isDead() {
        return dead;
    }

    public void die() {
        this.dead = true;
    }

    public Integer getHP() {
        return hp;
    }

    public void setPosition(Vector2i position) {
        this.position = position;
    }

    public void moveBy(Vector2i delta) {
        this.position = this.position.add(delta);
    }

    public Action getCurrentAction() {
        return currentAction;
    }

    public Action nextAction() {
        if (ai != null) {
            currentAction = ai.nextAction();
            return currentAction;
        } else {
            return null;
        }
    }

    public boolean hasAction() {
        return currentAction != null;
    }

    public void removeAction() {
        this.currentAction = null;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public void clearVision() {
        positionsVisible.clear();
    }

    public void addVisiblePosition(Vector2i point) {
        positionsVisible.add(point);
    }

    public boolean isPositionVisible(Vector2i point) {
        return positionsVisible.contains(point);
    }

    public void setVisionRayTracer(VisionRayTracer visionRayTracer) {
        this.visionRayTracer = visionRayTracer;
    }

    public void updateVision() {
        if (visionRayTracer != null) {
            visionRayTracer.calculateVision();
        }
    }

    public Stance getStance() {
        return stance;
    }

    public void stand() {
        this.stance = Stance.STANDING;
    }

    public void crouch() {
        this.stance = Stance.CROUCHING;
    }

    public Weapon getCurrentWeapon() {
        return currentWeapon;
    }

    public boolean canUseWeapon(TimePoint now) {
        return currentWeapon != null && currentWeapon.canUse(now);
    }

    @Override
    public String toString() {
        return "Entity " + ID;
    }
}
