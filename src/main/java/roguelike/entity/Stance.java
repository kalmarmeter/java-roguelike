package roguelike.entity;

public enum Stance {
    CROUCHING,
    STANDING
}
