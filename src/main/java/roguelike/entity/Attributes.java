package roguelike.entity;

public class Attributes {
    Integer strength;
    Integer agility;

    public Attributes(Integer strength, Integer agility) {
        this.strength = strength;
        this.agility = agility;
    }

    public Integer getStrength() {
        return strength;
    }

    public void setStrength(Integer strength) {
        this.strength = strength;
    }

    public Integer getAgility() {
        return agility;
    }

    public void setAgility(Integer agility) {
        this.agility = agility;
    }
}
