package roguelike.sound;

import roguelike.core.Random;
import roguelike.core.Vector2i;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SoundManager {
    static Map<String, List<String>> sounds = new HashMap<String, List<String>>();

    public static void play(String name, int volume) {
        Sound sound = new Sound(Random.randomElement(sounds.get(name)));
        sound.setVolume(volume);
        sound.play();
    }

    public static void play(String name, int maxVolume, Vector2i source, Vector2i listener) {
        int volume = maxVolume - (int)(listener.distanceTo(source) * 1.5);
    }

    public static void add(String name, String path) {
        List<String> paths = sounds.get(name);
        if (paths == null) {
            paths = new ArrayList<String>();
            sounds.put(name, paths);
        }
        paths.add(path);
    }

}
