package roguelike.sound;


import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import java.io.File;

public class Sound {
    String path;
    Clip clip;

    public Sound(String path) {
        this.path = path;
        try {
            AudioInputStream stream = AudioSystem.getAudioInputStream(new File(path));
            clip = AudioSystem.getClip();

            clip.open(stream);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void play() {
        clip.start();
        clip.setFramePosition(0);
    }

    public void setVolume(int volume) {
        FloatControl gainControl = (FloatControl)clip.getControl(FloatControl.Type.MASTER_GAIN);
        float gain = volume / 100.0f;
        float dB = (float) (Math.log(gain) / Math.log(10.0) * 20.0);
        gainControl.setValue(dB);
    }

}
