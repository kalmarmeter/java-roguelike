package roguelike.text;

import com.googlecode.lanterna.TextColor;
import roguelike.core.Pair;
import roguelike.graphics.Colors;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Text {

    public static String color(String string, TextColor color) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(Colors.toRGBTag(color));
        stringBuilder.append(string);
        stringBuilder.append("<>");
        return stringBuilder.toString();
    }

    public static List<Pair<String, TextColor>> parseColoredString(String string) {
        List<Pair<String, TextColor>> result = new ArrayList<Pair<String, TextColor>>();
        Pattern colorPattern = Pattern.compile(
                                "<([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])," +
                                "([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])," +
                                "([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])>" +
                                "|<>");
        Matcher colorMatcher = colorPattern.matcher(string);
        int start = 0;
        TextColor currentColor = Colors.WHITE;

        while (colorMatcher.find()) {
            int end = colorMatcher.start();
            if (start < end) {
                result.add(new Pair<String, TextColor>(string.substring(start, end), currentColor));
            }
            String colorTag = colorMatcher.group();
            if (!colorTag.matches("<>")) {
                currentColor = Colors.fromRGBTag(colorTag);
            } else {
                currentColor = Colors.WHITE;
            }
            start = colorMatcher.end();
        }

        if (result.isEmpty()) {
            result.add(new Pair<String, TextColor>(string, Colors.WHITE));
        }

        if (start < string.length()) {
            result.add(new Pair<String, TextColor>(string.substring(start), Colors.WHITE));
        }

        return result;
    }

}
