package roguelike.ray;

import roguelike.core.CommonMath;
import roguelike.core.Vector2i;

import roguelike.map.Map;
import roguelike.map.TileHeight;
import roguelike.map.TileProperties;

import java.util.List;

public class LightTracer extends RayTracer {

    Vector2i currentLightPos;

    public LightTracer(Map map) {
        super(map);
    }

    protected boolean shouldStop(Vector2i position) {
        if (!map.getMapArea().contains(position)) {
            return true;
        }

        boolean solid = TileProperties.getTileProperty(map.getTile(position)).isSolid();
        TileHeight height = TileProperties.getTileProperty(map.getTile(position)).getHeight();

        return height == TileHeight.FULL;

    }

    protected void positionTraced(Vector2i position) {
        if (map.getMapArea().contains(position)) {
            map.getTile(position).setLightLevel((int)(100 - (currentLightPos.distanceTo(position) / 15.0) * 100));
        }
    }

    public void traceLights(List<Vector2i> lights) {
        for (Vector2i lightPos : lights) {
            currentLightPos = lightPos;
            trace(lightPos, 0.0, CommonMath.TWO_PI, 45, 30);
        }
    }
}
