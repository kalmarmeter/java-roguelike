package roguelike.ray;

import roguelike.core.CommonMath;
import roguelike.core.Random;
import roguelike.core.Vector2i;
import roguelike.map.Map;
import roguelike.map.TileHeight;
import roguelike.map.TileProperties;

public class BloodTracer extends RayTracer {

    public BloodTracer(Map map) {
        super(map);
    }

    protected boolean shouldStop(Vector2i position) {
        if (!map.getMapArea().contains(position)) {
            return true;
        }
        boolean solid = TileProperties.getTileProperty(map.getTile(position)).isSolid();
        TileHeight height = TileProperties.getTileProperty(map.getTile(position)).getHeight();
        return solid || height != TileHeight.GROUND;
    }

    protected void positionTraced(Vector2i position) {
        if (!map.getMapArea().contains(position)) {
            return;
        }

        if (TileProperties.getTileProperty(map.getTile(position)).getHeight() != TileHeight.FULL && Random.oneIn(2)) {
            map.getTile(position).setBloody(true);
        }
    }

    public void splatterBlood(Vector2i position, double angle) {
        trace(position, angle, CommonMath.fromDegrees(120.0), 4, 9);
    }
}
