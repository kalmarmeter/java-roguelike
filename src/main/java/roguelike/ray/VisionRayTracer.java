package roguelike.ray;

import roguelike.core.CommonMath;
import roguelike.core.Vector2i;
import roguelike.entity.Entity;
import roguelike.entity.Stance;
import roguelike.map.Map;
import roguelike.map.TileHeight;
import roguelike.map.TileProperties;

public class VisionRayTracer extends RayTracer {

    Entity entity;
    Integer range;

    public VisionRayTracer(Entity entity, Map map, Integer range) {
        super(map);
        this.entity = entity;
        this.range = range;
    }

    protected final boolean shouldStop(Vector2i position) {
        //return false;
        if (!map.getMapArea().contains(position)) {
            return true;
        }
        boolean solid = TileProperties.getTileProperty(map.getTile(position)).isSolid();
        TileHeight height = TileProperties.getTileProperty(map.getTile(position)).getHeight();
        Stance stance = entity.getStance();
        return height == TileHeight.FULL || (height == TileHeight.HALF && stance == Stance.CROUCHING);
    }

    protected final void positionTraced(Vector2i position) {
        entity.addVisiblePosition(position);
    }

    public void calculateVision() {
        entity.clearVision();
        trace(entity.getPosition(), 0.0, CommonMath.TWO_PI, 90, range * 2);
    }

}
