package roguelike.ray;

import roguelike.core.Vector2i;
import roguelike.map.Map;

public abstract class RayTracer {
    Map map;

    public RayTracer(Map map) {
        this.map = map;
    }

    protected abstract boolean shouldStop(Vector2i position);
    protected abstract void positionTraced(Vector2i position);

    protected void trace(Vector2i origin, double directionRadians, double angleRadians, int numRays, int maxSteps) {

        double originX = origin.getX() + 0.5;
        double originY = origin.getY() + 0.5;
        double angleStep = angleRadians / (double)numRays;
        double beginAngle = directionRadians - angleRadians / 2.0;

        for (int ray = 0; ray < numRays; ++ray) {
            double angle = beginAngle + ray * angleStep;
            double deltaX = Math.cos(angle) * 0.5;
            double deltaY = Math.sin(angle) * 0.5;

            double x = originX + deltaX;
            double y = originY + deltaY;

            int steps = 1;
            while (steps < maxSteps) {
                Vector2i position = new Vector2i((int)x, (int)y); //Maybe rounding would be better?
                positionTraced(position);

                if (shouldStop(position)) {
                    break;
                }

                x += deltaX;
                y += deltaY;
                ++steps;
            }
        }

    }
}
